import { Module } from "~/lib/api/Module"
import { Directory } from '~/lib/api/Filesystem'
import { TSFile } from '~/modules/typescript'
import { JSONFile } from '~/lib/api/JSON'
import { Model } from "~/lib/api/Model"
import { namedTypes } from 'ast-types'

export default class TypeORM extends Module {
  static modname = 'typeorm'
  static project: Project

  constructor(project: Project) {
    super(project, TypeORM)
  }

  static get backend(): typeof Module['backend'] {
    return {
      contents: [
        new EntitiesDir(),
        new MigrationsDir(),
        new RepositoriesDir(),
        new ORMConfigFile(this.project.backend)
      ],
      packageJson: {
        scripts: {
          'dev': 'tsnd --respawn --type-check index.ts',
          'typeorm': 'ts-node ./node_modules/typeorm/cli.js',
          'build': 'rimraf build && tsc && copyfiles templates/**/* build',
          'start': 'ts-node index.ts',
          'makemigrations': 'yarn typeorm migration:generate -n',
          'migrate': 'yarn typeorm migration:run',
          'revert': 'yarn typeorm migration:revert'
        },
        dependencies: {
          'pg': '^8.2.0',
          'reflect-metadata': '^0.1.10',
          'typeorm': '0.2.22',
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type Backend = Project['backend']

class ORMConfigFile extends JSONFile {
  name = 'ormconfig'
  contents: JSONFile['contents']

  constructor(backend: Backend) {
    super()
    this.contents = {
      'type': 'postgres',
      'host': 'localhost',
      'port': 5432,
      'username': 'morgan',
      'password': '',
      'database': backend.name,
      'entities': ['./entities/*.ts'],
      'migrations': ['./migrations/*.ts'],
      'cli': {
        'entitiesDir': './entities',
        'migrationsDir': './migrations'
      }
    }
  }
}

class EntitiesDir extends Directory {
  name = 'entities'
}

class MigrationsDir extends Directory {
  name = 'migrations'
}

class RepositoriesDir extends Directory {
  name = 'repositories'
}

class RepositoryFile extends TSFile {
  model: Model

  constructor(model: Model) {
    super()
    this.model = model
  }

  get name() {
    return this.model.name
  }
}

export class EntityFile extends TSFile {
  model: Model

  constructor(model: Model) {
    super()
    this.model = model
    this.typescriptable.program.body = [
      ...this.imports,
      this.defaultExport
    ]
  }

  get name() {
    return this.model.name
  }

  get relations() {
    return this.model.relations
  }

  get columns() {
    return this.model.columns
  }

  get imports(): namedTypes.ImportDeclaration[] {
    return [
      ...this.defaultImports,
      ...this.relationImports
    ]
  }

  get relationSpecifiers(): namedTypes.ImportSpecifier[] {
    const b = this.typescriptable.b
    return this.relations.map((rel: Relation) => b.importSpecifier(b.identifier(rel.kind), b.identifier(rel.kind)))
  }

  buildSpecifier(name: string): namedTypes.ImportSpecifier {
    const b = this.typescriptable.b
    return b.importSpecifier(b.identifier(name), b.identifier(name))
  }

  get typeormSpecifiers(): namedTypes.ImportSpecifier[] {
    const column = this.columns.length ? [this.buildSpecifier('Column')] : []
    const entity = [this.buildSpecifier('Entity')]

    return [...column, ...entity, ...this.relationSpecifiers]
  }

  get baseEntityImport(): namedTypes.ImportDeclaration {
    const b = this.typescriptable.b
    return b.importDeclaration(
      [b.importDefaultSpecifier(b.identifier('BaseEntity'))],
      b.stringLiteral('../helpers/model')
    )
  }

  get defaultImports(): namedTypes.ImportDeclaration[] {
    const b = this.typescriptable.b
    return [
      b.importDeclaration(this.typeormSpecifiers, b.stringLiteral('typeorm')),
      this.baseEntityImport
    ]
  }

  get relationImports(): namedTypes.ImportDeclaration[] {
    const b = this.typescriptable.b
    return this.relations.map((rel: Relation) => b.importDeclaration(
      [b.importDefaultSpecifier(b.identifier(rel.foreignModel.capitalName))],
      b.stringLiteral(`./${rel.foreignModel.zapp.name}`)
    ))
  }

  get columnProperties(): namedTypes.ClassProperty[] {
    const b = this.typescriptable.b
    return this.columns.map((col: Column) => b.classProperty.from({
      key: b.identifier(col.name),
      value: null,
      typeAnnotation: b.tsTypeAnnotation({
        string: b.tsStringKeyword(),
        boolean: b.tsBooleanKeyword()
      }[col.type]),
      decorators: [
        b.decorator(b.callExpression(b.identifier('Column'), []))
      ]
    }))
  }

  get relationProperties(): namedTypes.ClassProperty[] {
    const b = this.typescriptable.b
    return this.relations.map((rel: Relation) => {
      const opts = rel.options ?? {}
      const optKeys: string[] = Object.keys(opts)
      const options = optKeys.reduce((options: namedTypes.ObjectExpression[], key: string) => {
        const value = opts[key as RelOptKey]

        const literal = typeof value === 'string'
          ? b.stringLiteral(value)
          : typeof value === 'boolean'
            ? b.booleanLiteral(value)
            : b.stringLiteral(value)

        return [...options, b.objectExpression([
          b.objectProperty(b.identifier(key), literal)
        ])]
      }, [])
      return b.classProperty.from({
        key: b.identifier(rel.name),
        value: null,
        typeAnnotation: b.tsTypeAnnotation(b.tsTypeReference(b.identifier(rel.foreignModel.capitalName))),
        static: false,

        decorators: [b.decorator(b.callExpression(b.identifier(rel.kind), [
          b.arrowFunctionExpression([b.identifier('type')], b.identifier(rel.foreignModel.capitalName)),
          b.arrowFunctionExpression(
            [b.identifier(rel.name)],
            b.memberExpression(b.identifier(rel.name), b.identifier(rel.foreignProp ?? this.model.pluralName), false)
          ),
          ...options
        ]))]
      })
    })
  }

  get classProperties(): namedTypes.ClassProperty[] {
    return this.columnProperties.concat(this.relationProperties)
  }

  get defaultExport(): namedTypes.ExportDefaultDeclaration {
    const b = this.typescriptable.b
    return b.exportDefaultDeclaration(b.classDeclaration.from({
      id: b.identifier(this.model.capitalName),
      body: b.classBody(this.classProperties),
      superClass: b.identifier('BaseEntity'),
      decorators: [
        b.decorator(b.callExpression(b.identifier('Entity'), [b.stringLiteral(this.model.name)]))
      ]
    }))
  }
}

type RelationKind = 'ManyToOne' | 'OneToMany'

interface Relation {
  name: string
  kind: RelationKind
  foreignModel: typeof Model
  foreignProp?: string
  options?: RelOpt
}

interface RelOpt {
  cascade?: boolean | 'insert' | 'update'
  onDelete?: 'CASCADE'
}

type RelOptKey = keyof Relation['options']

interface Column {
  name: string
  type: 'string' | 'boolean'
  options?: {
    nullable?: true
  }
}

export class Entity extends Model {
  typeorm: {
    entityFile: EntityFile
    repoFile: RepositoryFile
  }

  constructor(project: Project, extension: typeof Model) {
    super(project, extension)
    this.typeorm = {
      entityFile: new EntityFile(this),
      repoFile: new RepositoryFile(this)
    }
    const entitiesDir = project.backend.findDirectory('entities')
    if (entitiesDir) {
      entitiesDir.withFiles([this.typeorm.entityFile])
    }
    const reposDir = project.backend.findDirectory('repositories')
    if (reposDir) {
      reposDir.withFiles([this.typeorm.repoFile])
    }
  }
}

