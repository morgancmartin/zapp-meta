export default function overmind({$overmind: $overmind, beforeNuxtRender: beforeNuxtRender}: any) {
  if (process.server) {
    beforeNuxtRender(({ nuxtState }: any) => nuxtState.__OVERMIND_MUTATIONS = $overmind.hydrate());
  }
}
