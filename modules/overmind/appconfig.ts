import { merge, namespaced } from 'overmind/config';
import { state } from './state';
import * as actions from './actions';
import * as effects from './effects';
import pages from '~/overmind/pages';
import components from '~/overmind/components';
import API from '~/overmind/api';

export const config = merge({
  state: state,
  actions: actions,
  effects: effects
}, namespaced({
  ...pages,
  ...components,
  API: API
}));
