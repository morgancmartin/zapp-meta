// @ts-nocheck
import { Action, AsyncAction, rehydrate } from 'overmind';

export const initializeGql: Action = ({effects: effects}: any) => {
  const httpUrl = process.env.GRAPHQL_HTTP_URL;
  const wsUrl = process.env.GRAPHQL_WS_URL;

  effects.gql.initialize({
    endpoint: httpUrl
  }, {
    endpoint: wsUrl
  });
};

export const initializeClient: Action = ({ state, actions }: any) => {
  const mutations = (window as any).__NUXT__.__OVERMIND_MUTATIONS;
  actions.initializeGql();
  rehydrate(state, mutations);
};

export const initializeServer: Action = ({actions: actions}: any) => actions.initializeGql();

export const arrToIdObj: Action<{items: { [id: string]: string }[]}> = (_: any, { items }) => {
  return items.reduce((result: any, item: any) => Object.assign(result, {
    [item.id]: item
  }), {});
};

export const hydrateState = ({state: state}: any, hydration: any) => Object.assign(state, hydration);
