import { graphql } from 'overmind-graphql';
import { GraphQLNormalizr } from 'graphql-normalizr';
import * as queries from './queries';
import * as mutations from './mutations';
import * as subscriptions from './subscriptions';

const {
  addRequiredFields: addRequiredFields
} = new GraphQLNormalizr();

Object.values(queries).forEach(query => addRequiredFields(query));
Object.values(mutations).forEach(mutation => addRequiredFields(mutation));

export const gql = graphql({
  queries: queries,
  mutations: mutations,
  subscriptions: subscriptions
});
