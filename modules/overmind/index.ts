import { Module } from "~/lib/api/Module";
import { Directory } from "~/lib/api/Filesystem";
import { namedTypes } from "ast-types";
import { TSFile, TSFSFile } from "~/modules/typescript";
import { NuxtContributor } from "~/modules/nuxt";
import { Mixin } from "ts-mixer";
import { AnyPage } from "~/lib/api/Page";

export default class OvermindModule extends Mixin(Module, NuxtContributor) {
  static modname = "overmind";

  static project: Project;

  constructor(project: Project) {
    super(project, OvermindModule);

    const middlewareDir = project.frontend.findDirectory("middleware");
    if (middlewareDir instanceof Directory) {
      middlewareDir.withFiles([new NuxtMiddlewareFile()]);
    }

    const pluginsDir = project.frontend.findDirectory("plugins");
    if (pluginsDir instanceof Directory) {
      pluginsDir.withFiles([new NuxtPluginFile()]);
    }
  }

  static complete() {
    // const gqlDir = this.project.frontend.findDirectory('overmind')?.findDirectory('effects')?.findDirectory('gql')
    // const queries = gqlDir?.findFile('queries')
    // const mutations = gqlDir?.findFile('mutations')
    // const subscriptions = gqlDir?.findFile('subscriptions')

    // console.log(Boolean(queries), Boolean(mutations), Boolean(subscriptions))

    this.project.frontend.withFiles([new OvermindDir(this.project)]);
  }

  static get frontend() {
    return {
      contents: [new TypesDir()],
      packageJson: {
        scripts: {
          dev: "concurrently 'nuxt-ts' 'overmind-devtools'",
        },
        dependencies: {
          overmind: "^23.1.0-1589434363117",
          "overmind-graphql": "^3.1.0-1589434363117",
          "overmind-vue": "^23.1.0-1589434363117",
          "graphql-normalizr": "^2.10.1",
        },
        devDependencies: {
          "overmind-devtools": "^24.1.0",
          "@babel/runtime-corejs2": "^7.9.6",
          "core-js": "2",
          "@types/phoenix": "^1.4.4",
          apollo: "^2.27.4",
          concurrently: "^5.2.0",
        },
      },
    };
  }

  static get nuxt() {
    return {
      extendConfig(
        nuxtConfig: NuxtConfig,
        b: Builders
      ): ReturnType<NuxtConfigGetter> {
        const routerConfig = b.objectProperty(
          b.identifier("router"),
          b.objectExpression([
            b.objectProperty(
              b.identifier("middleware"),
              b.stringLiteral("overmind")
            ),
          ])
        );

        return {
          visitObjectProperty(path: any) {
            const node = path.node;
            if (
              namedTypes.Identifier.check(node.key) &&
              node.key.name === "plugins"
            ) {
              node.value.elements.push(
                b.stringLiteral.from({ value: "~/plugins/overmind" })
              );
            }
            return false;
          },
          visitAssignmentExpression(path: any) {
            const node = path.node;
            const leftIsMemberExp = namedTypes.MemberExpression.check(
              node.left
            );
            const leftObjIsModule = node.left.object.name === "module";
            const leftPropIsExports = node.left.property.name === "exports";
            if (leftIsMemberExp && leftObjIsModule && leftPropIsExports) {
              node.right.properties.push(routerConfig);
            }
            this.traverse(path);
          },
        };
      },
    };
  }
}

type Project = ConstructorParameters<typeof Module>[0];
type NuxtConfigGetter = typeof NuxtContributor["nuxt"]["extendConfig"];
type NuxtConfigGetterParams = Parameters<NuxtConfigGetter>;
type NuxtConfig = NuxtConfigGetterParams[0];
type Builders = NuxtConfigGetterParams[1];

class NuxtMiddlewareFile extends TSFSFile {
  name = "overmind";

  constructor() {
    super("modules/overmind/nuxtmiddleware.ts");
  }
}

class NuxtPluginFile extends TSFSFile {
  name = "overmind";

  constructor() {
    super("modules/overmind/nuxtplugin.ts");
  }
}

class OvermindDir extends Directory {
  name = "overmind";
  contents = [
    new AppConfigFile(),
    new GlobalActions(),
    new Helpers(),
    new GlobalOperators(),
    new GlobalState(),
    new APIDir(),
    new EffectsDir(),
  ];

  constructor(project: Project) {
    super();
    this.withFiles([new ComponentsDir(project), new PagesDir(project)]);
  }
}

class GlobalActions extends TSFSFile {
  name = "actions";

  constructor() {
    super("modules/overmind/globalactions.ts");
  }
}

class Helpers extends TSFSFile {
  name = "helpers";

  constructor() {
    super("modules/overmind/helpers.ts");
  }
}

class GlobalOperators extends TSFSFile {
  name = "operators";

  constructor() {
    super("modules/overmind/globaloperators.ts");
  }
}

class GlobalState extends TSFSFile {
  name = "state";

  constructor() {
    super("modules/overmind/globalstate.ts");
  }
}

class APIDir extends Directory {
  name = "api";
  contents = [new APIActions(), new APIState(), new APIIndex()];
}

class APIActions extends TSFSFile {
  name = "actions";

  constructor() {
    super("modules/overmind/apiactions.ts");
  }
}

class APIState extends TSFSFile {
  name = "state";

  constructor() {
    super("modules/overmind/apistate.ts");
  }
}

class APIIndex extends TSFSFile {
  name = "index";

  constructor() {
    super("modules/overmind/apiindex.ts");
  }
}

class EffectsDir extends Directory {
  name = "effects";
  contents = [new EffectsIndex(), new GQLDir()];
}

class EffectsIndex extends TSFSFile {
  name = "index";

  constructor() {
    super("modules/overmind/effectsindex.ts");
  }
}

class GQLDir extends Directory {
  name = "gql";
  contents = [
    new GQLIndex(),
    new GQLQueries(),
    new GQLMutations(),
    new GQLSubscriptions(),
  ];
}

class GQLIndex extends TSFSFile {
  name = "index";

  constructor() {
    super("modules/overmind/gqlindex.ts");
  }
}

export class GQLFile extends TSFile {
  name: string;

  constructor(name: string) {
    super();
    this.name = name;
  }
}

export class GQLQueries extends TSFSFile {
  name = "queries";

  constructor() {
    super("modules/overmind/gqlqueries.ts");
  }
}

export class GQLMutations extends TSFSFile {
  name = "mutations";

  constructor() {
    super("modules/overmind/gqlmutations.ts");
  }
}

export class GQLSubscriptions extends TSFSFile {
  name = "subscriptions";

  constructor() {
    super("modules/overmind/gqlsubscriptions.ts");
  }
}

class ComponentsDir extends Directory {
  name = "components";
  project: Project;
  _contents: Directory["contents"] = [];

  get contents() {
    if (!this._contents) {
      this.contents = [new ComponentsIndex(this.project)];
    }
    return this._contents;
  }

  set contents(contents: Directory["contents"]) {
    this._contents = contents;
  }

  constructor(project: Project) {
    super();
    this.project = project;
  }
}

class ComponentsIndex extends TSFSFile {
  name = "index";

  constructor(project: Project) {
    super("modules/overmind/componentsindex.ts");
  }
}

class PagesDir extends Directory {
  name = "pages";

  constructor(project: Project) {
    super();
    this.project = project;
    this.withFiles(this.directories);
  }

  _directories?: Directory["contents"];
  get directories() {
    if (!this._directories) {
      this._directories = [new PagesIndex(this.project), ...this.pageDirs];
    }
    return this._directories;
  }

  set directories(directories: Directory["contents"]) {
    this._directories = directories;
  }

  _pageDirs?: Directory["contents"];
  get pageDirs() {
    if (!this._pageDirs) {
      this._pageDirs = Object.values(this.project.pages).map(
        (page: AnyPage) => new PageDir(page)
      );
    }
    return this._pageDirs;
  }
  project: Project;
}

class PagesIndex extends TSFSFile {
  name = "index";

  constructor(project: Project) {
    super("modules/overmind/pagesindex.ts");
  }
}

class PageDir extends Directory {
  name: string;
  page: AnyPage;

  constructor(page: AnyPage) {
    super();
    this.name = `${page.capitalName}Page`;
    this.page = page;
    this.withFiles(this.files);
  }

  _files?: Directory["contents"];
  get files() {
    if (!this._files) {
      this._files = [
        new ChartIndex(this),
        new ChartActions(this),
        new ChartState(this),
      ];
    }
    return this._files;
  }
}

class ChartFile extends TSFile {
  // @ts-ignore
  abstract name: string;
  dir: PageDir;
  get b() {
    return this.typescriptable.b;
  }

  constructor(dir: PageDir) {
    super();
    this.dir = dir;
    this.mergeBody = this.getBody();
  }

  // @ts-ignore
  abstract getBody(): TSFile["_body"];
}

class ChartIndex extends ChartFile {
  name = "index";

  getBody() {
    return [...this.imports, this.config, this.chart, this.export];
  }

  get imports() {
    const b = this.b;
    return [
      b.importDeclaration(
        [
          b.importSpecifier(
            b.identifier("Statechart"),
            b.identifier("Statechart")
          ),
          b.importSpecifier(
            b.identifier("statechart"),
            b.identifier("statechart")
          ),
        ],
        b.stringLiteral("overmind/config")
      ),
      b.importDeclaration(
        [b.importSpecifier(b.identifier("state"), b.identifier("state"))],
        b.stringLiteral("./state")
      ),
      b.importDeclaration(
        [b.importNamespaceSpecifier(b.identifier("actions"))],
        b.stringLiteral("./actions")
      ),
    ];
  }

  get config() {
    const b = this.b;
    return b.variableDeclaration("const", [
      b.variableDeclarator(
        b.identifier("config"),
        b.objectExpression([
          b.objectProperty(b.identifier("state"), b.identifier("state")),
          b.objectProperty(b.identifier("actions"), b.identifier("actions")),
        ])
      ),
    ]);
  }

  get chart() {
    const b = this.b;
    return b.variableDeclaration("const", [
      b.variableDeclarator(
        b.identifier.from({
          name: "chart",

          typeAnnotation: b.tsTypeAnnotation(
            b.tsTypeReference(
              b.identifier("Statechart"),
              b.tsTypeParameterInstantiation([
                b.tsTypeQuery(b.identifier("config")),
                b.tsTypeLiteral([
                  b.tsPropertySignature(
                    b.identifier("INITIAL"),
                    b.tsTypeAnnotation(b.tsVoidKeyword())
                  ),
                ]),
              ])
            )
          ),
        }),
        b.objectExpression([
          b.objectProperty(b.identifier("initial"), b.stringLiteral("INITIAL")),
          b.objectProperty(
            b.identifier("states"),
            b.objectExpression([
              b.objectProperty(
                b.identifier("INITIAL"),
                b.objectExpression([
                  b.objectProperty(
                    b.identifier("on"),
                    b.objectExpression([
                      b.objectProperty(
                        b.identifier("updateState"),
                        b.nullLiteral()
                      ),
                    ])
                  ),
                ])
              ),
            ])
          ),
        ])
      ),
    ]);
  }

  get export() {
    const b = this.b;
    return b.exportDefaultDeclaration(
      b.callExpression(b.identifier("statechart"), [
        b.identifier("config"),
        b.objectExpression([
          b.objectProperty(b.identifier("chart"), b.identifier("chart")),
        ]),
      ])
    );
  }
}

class ChartActions extends ChartFile {
  name = "actions";

  getBody() {
    return [...this.imports, this.namespace, ...this.actions];
  }

  get imports() {
    const b = this.b;
    return [
      b.importDeclaration(
        [
          b.importSpecifier(b.identifier("Action"), b.identifier("Action")),
          b.importSpecifier(
            b.identifier("AsyncAction"),
            b.identifier("AsyncAction")
          ),
        ],
        b.stringLiteral("overmind")
      ),
      b.importDeclaration(
        [b.importSpecifier(b.identifier("State"), b.identifier("State"))],
        b.stringLiteral("./state")
      ),
    ];
  }

  get namespace() {
    const b = this.b;
    return b.variableDeclaration("const", [
      b.variableDeclarator(
        b.identifier("nameSpace"),
        b.stringLiteral("SearchResults")
      ),
    ]);
  }

  get actions() {
    const b = this.b;
    return [
      b.exportNamedDeclaration(
        b.variableDeclaration("const", [
          b.variableDeclarator(
            b.identifier.from({
              name: "updateState",

              typeAnnotation: b.tsTypeAnnotation(
                b.tsTypeReference(
                  b.identifier("Action"),
                  b.tsTypeParameterInstantiation([
                    b.tsTypeReference(b.identifier("State")),
                  ])
                )
              ),
            }),
            b.arrowFunctionExpression(
              [
                b.objectPattern.from({
                  properties: [
                    b.objectProperty(
                      b.identifier("state"),
                      b.identifier("state")
                    ),
                  ],
                  typeAnnotation: b.tsTypeAnnotation(b.tsAnyKeyword()),
                }),
                b.identifier("input"),
              ],
              b.blockStatement([
                b.expressionStatement(
                  b.callExpression(
                    b.memberExpression(
                      b.identifier("Object"),
                      b.identifier("assign"),
                      false
                    ),
                    [
                      b.memberExpression(
                        b.identifier("state"),
                        b.identifier("nameSpace"),
                        true
                      ),
                      b.identifier("input"),
                    ]
                  )
                ),
              ])
            )
          ),
        ]),
        [],
        null
      ),
    ];
  }
}

class ChartState extends ChartFile {
  name = "state";

  getBody() {
    return [...this.imports, this.typeDef, ...this.functions, this.state];
  }

  get imports() {
    const b = this.b;
    return [
      b.importDeclaration(
        [
          b.importSpecifier(
            b.identifier("makeDerivation"),
            b.identifier("makeDerivation")
          ),
        ],
        b.stringLiteral("~/overmind/helpers")
      ),
    ];
  }

  get typeDef() {
    const b = this.b;
    return b.exportNamedDeclaration(
      b.tsTypeAliasDeclaration(
        b.identifier("State"),
        b.tsTypeLiteral([
          b.tsPropertySignature(
            b.identifier("destinationId"),
            b.tsTypeAnnotation(b.tsStringKeyword())
          ),
          b.tsPropertySignature(
            b.identifier("destination"),
            b.tsTypeAnnotation(b.tsAnyKeyword())
          ),
        ])
      ),
      [],
      null
    );
  }

  get functions() {
    const b = this.b;
    return [
      b.functionDeclaration(
        b.identifier("getDestination"),
        [
          b.objectPattern.from({
            properties: [
              b.objectProperty(
                b.identifier("destinationId"),
                b.identifier("destinationId")
              ),
            ],

            typeAnnotation: b.tsTypeAnnotation(
              b.tsTypeLiteral([
                b.tsPropertySignature(
                  b.identifier("destinationId"),
                  b.tsTypeAnnotation(
                    b.tsUnionType([b.tsStringKeyword(), b.tsUndefinedKeyword()])
                  )
                ),
              ])
            ),
          }),
          b.identifier.from({
            name: "state",
            typeAnnotation: b.tsTypeAnnotation(b.tsAnyKeyword()),
          }),
        ],
        b.blockStatement([
          b.returnStatement(
            b.conditionalExpression(
              b.identifier("destinationId"),
              b.memberExpression(
                b.memberExpression(
                  b.identifier("state"),
                  b.identifier("destinations"),
                  false
                ),
                b.identifier("destinationId"),
                true
              ),
              b.objectExpression([])
            )
          ),
        ]),
        false
      ),
    ];
  }

  get state() {
    const b = this.b;
    return b.exportNamedDeclaration(
      b.variableDeclaration("const", [
        b.variableDeclarator(
          b.identifier.from({
            name: "state",
            typeAnnotation: b.tsTypeAnnotation(
              b.tsTypeReference(b.identifier("State"))
            ),
          }),
          b.objectExpression([
            b.objectProperty(
              b.identifier("destinationId"),
              b.stringLiteral("")
            ),
            b.objectProperty(
              b.identifier("destination"),
              b.callExpression(b.identifier("makeDerivation"), [
                b.identifier("getDestination"),
                b.objectExpression([]),
              ])
            ),
          ])
        ),
      ]),
      [],
      null
    );
  }
}

class TypesDir extends Directory {
  name = "types";
  contents = [new GqlNormalizrDeclaration()];
}

class GqlNormalizrDeclaration extends TSFSFile {
  name = "graphql-normalizr.d";

  constructor() {
    super("modules/overmind/graphql-normalizr.d.ts");
  }
}

class AppConfigFile extends TSFSFile {
  name = "appconfig";

  constructor() {
    super("modules/overmind/appconfig.ts");
  }
}
