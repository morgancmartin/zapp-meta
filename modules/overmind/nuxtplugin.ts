import Vue from 'vue';
import { createOvermindSSR, createOvermind, IConfig } from 'overmind';
import { createPlugin } from 'overmind-vue';
import { Plugin } from '@nuxt/types';
import { GraphQLNormalizr } from 'graphql-normalizr';
import { config } from '~/overmind/appconfig';
import { normalizeData } from '~/utils/normalizr';
let overmind: any;

const OvermindPlugin: Plugin = context => {
  overmind = process.server ? createOvermindSSR(config) : createOvermind(config);
  Vue.use(createPlugin(overmind));
  context.$overmind = overmind;
  context.$hydrate = getHydrator(context);

  if (process.server) {
    overmind.actions.initializeServer();
  } else {
    overmind.actions.initializeClient();
  }
};

export default OvermindPlugin;

declare module 'overmind' {
  interface Config {}
}

declare module 'vue/types/vue' {
  interface Config {}
  interface Vue {}
}

declare module '@nuxt/types' {
  interface Context {
    $overmind: any;
    $hydrate: any;
  }
}

function getHydrator(
  {
    $overmind: {
      actions: actions,
      state: state
    }
  }: any
) {
  const hydrate = (data: any) => {
    const hydration = normalizeData(data);

    if (process.server) {
      Object.assign(state, hydration);
      return hydration;
    } else {
      actions.hydrateState(hydration);
    }
  };

  return hydrate;
}
