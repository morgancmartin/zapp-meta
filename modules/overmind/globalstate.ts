import { makeRecordList } from '~/overmind/helpers';

type Derivations = {
  liveaboardList: any[],
  destinationList: any[]
};

const derivations: Derivations = {
  liveaboardList: makeRecordList('liveaboards'),
  destinationList: makeRecordList('destinations')
};

type Records = {
  liveaboards: any
};

const records: Records = {
  liveaboards: {}
};

type State = Records & Derivations;

export const state: State = {
  ...records,
  ...derivations
};
