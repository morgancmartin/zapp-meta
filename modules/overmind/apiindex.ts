import { Statechart, statechart } from 'overmind/config';
import { state } from './state';
import * as actions from './actions';

const config = {
  state: state,
  actions: actions
};

const apiChart: Statechart<typeof config, {
  INITIAL: void,
  CREATE_LOADING: void,
  DELETE_LOADING: void,
  UPDATE_LOADING: void
}> = {
  initial: 'INITIAL',

  states: {
    INITIAL: {
      on: {}
    },

    CREATE_LOADING: {
      on: {}
    },

    DELETE_LOADING: {
      on: {}
    },

    UPDATE_LOADING: {
      on: {}
    }
  }
};

export default statechart(config, {
  apiChart: apiChart
});
