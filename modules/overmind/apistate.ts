type MutationTarget = {
  input: any,
  result: any,
  error: Error | null,
  resource: any | null
};

const mutationTargetState = {
  input: null,
  result: null,
  error: null,
  resource: null
};

type APIState = {};
const apiState: APIState = {};
type State = APIState;

export const state: State = {
  ...apiState
};
