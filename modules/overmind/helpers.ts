import { derived, IState } from 'overmind';
import { find, merge } from 'lodash';

export function makeRecordList(key: string) {
  return makeDerivation((_: any, state: any) => {
    const records = state[key];
    return records ? Object.values(records) : [];
  });
}

export function makeDerivation(handler: any, defaultVal?: any) {
  return process.server ? defaultVal ?? [] : derived(handler);
}
