import { Operator, map, mutate } from 'overmind'
import { normalizeData } from '~/utils/normalizr'

export const injectPageName = (pageName: string) => {
  return map(function injectPageName(_, input: any) {
    return {
      ...input,
      pageName: pageName
    }
  })
}

export const fireGqlMutation = (mutationName: string) => {
  return map(function fireGqlMutation({ effects }: any, input: any) {
    const mutationResult = effects.gql.mutations[mutationName]({
      input: input
    })

    return {
      input: input,
      mutationResult: mutationResult
    }
  })
}

export const selectStateTarget = (selector: any) => {
  return map(function selectStateTarget({state: state}: any, input: any = {}) {
    return {
      target: selector(state),
      ...input
    }
  })
}

export const awaitAndUpdateTarget = () => {
  return mutate(async function callGqlMutationWithInput(_, { input, mutationResult, target }: any) {
    Object.assign(target, {
      input: input,
      result: await mutationResult
    })
  })
}

export const resolveGqlCreation = (collectionName: string, relationKeys: any[]) => {
  return mutate(function resolveGqlCreation({ state }: any, { target }: any) {
    const {
      input: input,
      result: result
    } = target

    const resources = normalizeData(result)

    Object.entries(resources).forEach(([collectionName, value]: [string, any]) => {
      Object.entries(value).forEach(([id, value]: [string, any]) => {
        state[collectionName][id] = value
      })
    })

    const resource: any = Object.values(resources[collectionName])[0]
    target.resource = resource

    relationKeys.forEach(([collectionName, relationName, foreignRelationName]) => {
      const collection: any = state[collectionName]
      const relationInput = input[relationName]

      if (relationInput) {
        const relationId = relationInput.id
        const parent: any = collection[relationId]

        if (parent) {
          const foreignCollection: any[] = parent[foreignRelationName]

          if (foreignCollection) {
            foreignCollection.push(resource.id)
          }
        }
      }
    })
  })
}

export const resolveGqlDeletion = (mutationName: string, collectionName: string, relationKeys: any[]) => {
  return mutate(function resolveGqlDeletion({ state }: any, { target: { input, result } }: any) {
    const deleted = result[mutationName]

    if (!deleted) {
      throw new Error('GQL Deletion failed')
    }

    const resource = state[collectionName][input.id]

    relationKeys.forEach(([collectionName, relationName, foreignRelationName]) => {
      const collection: any = state[collectionName]
      const relationId = resource[relationName]
      const relation = collection[relationId]

      if (relation) {
        const foreignCollection: any[] = relation[foreignRelationName]

        if (foreignCollection) {
          foreignCollection.splice(foreignCollection.indexOf(input.id), 1)
        }
      }
    })

    delete state[collectionName][input.id]
  })
}

export const resolveGqlUpdate = (collectionName: string, relationKeys: any[]) => {
  return mutate(function resolveGqlUpdate({ state }: any, { target }: any) {
    const {
      input: input,
      result: result
    } = target

    const resources = normalizeData(result)

    Object.entries(resources).forEach(([collectionName, value]: [string, any]) => {
      Object.entries(value).forEach(([id, value]: [string, any]) => {
        state[collectionName][id] = value
      })
    })

    const resource: any = Object.values(resources[collectionName])[0]
    target.resource = resource

    relationKeys.forEach(([collectionName, relationName, foreignRelationName]) => {
      const collection: any = state[collectionName]
      const relationInput = input[relationName]

      if (relationInput) {
        const relationId = relationInput.id
        const parent: any = collection[relationId]

        if (parent) {
          const foreignCollection: any[] = parent[foreignRelationName]

          if (foreignCollection) {
            foreignCollection.push(resource.id)
          }
        }
      }
    })
  })
}

export const transition = (selector: any) => {
  return map(function transition({ actions }: any, input: any) {
    selector(actions)(input)
  })
}

export const rejectGqlMutation = () => {
  return mutate(function rejectGqlMutation(_, { error, target }: {error: Error, target: any}) {
    Object.assign(target, {
      error: error
    })
  })
}

export const catchMutationError = () => {
  return map(function catchMutationError(_, error: Error) {
    return { error }
  })
}
