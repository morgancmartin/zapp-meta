import { ApolloError } from 'apollo-server-express';
export const NOT_AUTHENTICATED = new ApolloError('You are not authenticated', 'NOT_AUTHENTICATED');

export function createInternalServerError(ticketNumber: number) {
  return new ApolloError('Internal Server Error', 'INTERNAL_SERVER_ERROR', {
    ticketNumber: ticketNumber
  });
}
