import { Connection } from 'typeorm';
import { IResolvers } from 'graphql-tools';
import { Request } from 'express';
import { PubSub } from 'graphql-subscriptions';

declare global {
  type IContext = {
    connection: Connection,
    req: Request,
    pubsub: PubSub
  };

  type IResolver = IResolvers<any, IContext>;
}
