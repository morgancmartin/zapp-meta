import { Module } from "~/lib/api/Module"
import { Directory } from '~/lib/api/Filesystem'
import { TSFSFile, TSConfigFile } from '~/modules/typescript'
import { DotenvFile } from '~/modules/dotenv'
import { Mixin } from 'ts-mixer'
import { NodeProjectContributor } from '~/lib/api/NodeProject'

export default class ExpressModule extends Mixin(Module, NodeProjectContributor) {
  static modname = 'express'
  static project: Project

  constructor(project: Project) {
    super(project, ExpressModule)
  }

  static get backend() {
    return {
      contents: [
        new HelpersDir(),
        new LibsDir(),
        new SeedDir(),
        new IndexFile(),
        new TSConfig(),
        new TypingsFile(),
        new Dotenv(this.project.backend)
      ],
      packageJson: {
        main: 'index.ts',
        dependencies: {
          '@types/node': '^14.0.1',
          'bcryptjs': '^2.4.3',
          'express': '^4.17.1',
          'faker': '^4.1.0',
          'glob': '^7.1.6',
          'jsonwebtoken': '^8.5.1',
          'lodash': '^4.17.15',
          'pg': '^8.2.0',
          'winston': '^3.2.1'
        },
        devDependencies: {
          '@types/bcryptjs': '^2.4.2',
          '@types/glob': '^7.1.1',
          '@types/jsonwebtoken': '^8.5.0',
          '@types/lodash': '^4.14.150',
          'copyfiles': '^2.2.0',
          'rimraf': '^3.0.2',
          'ts-node': '3.3.0',
          'ts-node-dev': '^1.0.0-pre.44',
          'tsconfig-paths': '^3.9.0',
          'typescript': '^3.9.3'
        },
        resolutions: {
          'tslib': '1.11.2'
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type Backend = Project['backend']

class HelpersDir extends Directory {
  name = 'helpers'
  contents = [
    new ErrorsFile(),
    new ModelFile()
  ]
}

class LibsDir extends Directory {
  name = 'libs'
  contents = [
    new LoggerFile()
  ]
}

class SeedDir extends Directory {
  name = 'seed'
}

class IndexFile extends TSFSFile {
  name = 'index'

  constructor() {
    super('modules/express/backendindex.ts')
  }
}

class Dotenv extends DotenvFile {
  contents: string[]

  constructor(backend: Backend) {
    super()
    this.contents = [
      'PORT=3001',
      'DB_PORT=5432',
      'DB_USER=morgan',
      'DB_PASS=',
      `DB_NAME=${backend.name}`,
      'DB_DROP=true',
      'DB_SYNC=true',
      'SEED_ENABLED=true',
      'LOGGER_FILE=output.log',
      'LOGGER_COLORIZE=true',
      'DEV=true'
    ]
  }
}

class TSConfig extends TSConfigFile {
  contents = {
    "compilerOptions": {
      "target": "es5",
      "module": "commonjs",
      "lib": [
        "es2015"
      ],
      "outDir": "./build",
      "strict": true,
      "strictNullChecks": true,
      "strictPropertyInitialization": false,
      "typeRoots": [
        "./types"
      ],
      "esModuleInterop": true,
      "experimentalDecorators": true,
      "emitDecoratorMetadata": true,
      "forceConsistentCasingInFileNames": true
    }
  }
}

class TypingsFile extends TSFSFile {
  name = 'typings.d'

  constructor() {
    super('modules/express/typings.d.ts')
  }
}

class ErrorsFile extends TSFSFile {
  name = 'errors'

  constructor() {
    super('modules/express/errors.ts')
  }
}

class ModelFile extends TSFSFile {
  name = 'model'

  constructor() {
    super('modules/express/model.ts')
  }
}

class LoggerFile extends TSFSFile {
  name = 'logger'

  constructor() {
    super('modules/express/logger.ts')
  }
}
