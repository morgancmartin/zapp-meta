require('dotenv').config();
import 'reflect-metadata';
import path from 'path';
import glob from 'glob';
import express from 'express';
import { createConnection } from 'typeorm';
import { ApolloError, ApolloServer, PubSub } from 'apollo-server-express';
import { createServer } from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import logger from './libs/logger';
import { createInternalServerError } from './helpers/errors';
main();

function main() {
  start().catch(onError);
}

async function start() {
  const app = express();
  const conn = await createDBConn();
  const apolloServer = createApolloServer(app, conn);
  const httpServer = createHttpServer(app, apolloServer);
  servePublic(app);
  await handleSeeds(conn);
  onReady(httpServer, apolloServer);
}

function servePublic(app: any) {
  app.use(express.static(path.resolve('./public')));
}

function createDBConn() {
  const {
    DB_PORT: port,
    DB_USER: username,
    DB_PASS: password,
    DB_NAME: database,
    DB_DROP: dropSchema,
    DB_SYNC: synchronize
  }: any = process.env;

  const type = 'postgres';
  const host = 'localhost';
  const entities = getDBDir('entities');
  const migrations = getDBDir('migrations');
  const subscribers = getDBDir('subscribers');

  return createConnection({
    type: type,
    host: host,
    port: port,
    username: username,
    password: password,
    database: database,
    entities: entities,
    migrations: migrations,
    subscribers: subscribers,
    dropSchema: dropSchema,
    synchronize: synchronize
  });
}

function getDBDir(name: string) {
  return [path.resolve(__dirname, `./${name}/**/*.ts`)];
}

function createApolloServer(app: any, connection: any) {
  const typeDefs = [], resolvers = [];
  const modules = glob.sync(path.resolve(__dirname, './graphql/**/*.ts'));

  for (let path of modules) {
    const module = require(path);

    if (module.typeDefs) {
      typeDefs.push(module.typeDefs);
    }

    resolvers.push(module.resolvers);
  }

  const pubsub = new PubSub();

  const server = new ApolloServer({
    typeDefs: typeDefs,
    resolvers: resolvers,

    context: (
      {
        req: req
      }
    ) => ({
      connection: connection,
      req: req,
      pubsub: pubsub
    }),

    formatError: error => {
      if (error.originalError instanceof ApolloError) {
        return error;
      }

      const ticketNumber = new Date().valueOf();
      const customError = createInternalServerError(ticketNumber);
      const printError = error;
      logger.error('#%d: %s', ticketNumber, printError);
      return customError;
    }
  });

  server.applyMiddleware({
    app: app,
    path: '/graphql'
  });

  return server;
}

function createHttpServer(app: any, apolloServer: ApolloServer) {
  const httpServer = createServer(app);
  apolloServer.installSubscriptionHandlers(httpServer);
  return httpServer;
}

async function handleSeeds(connection: any) {
  if (process.env.SEED_ENABLED) {
    const seeds = glob.sync(path.resolve(__dirname, './seed/**/*.ts'));
    logger.warn('Found %d seeders...', seeds.length);

    for (let path of seeds) {
      logger.warn('Running seed at path: %s...', path);
      const seed = require(path);

      for (let value of Object.values(seed)) {
        if (typeof value === 'function') {
          await value(connection, logger);
        }
      }
    }
  }
}

function onReady(httpServer: any, apolloServer: ApolloServer) {
  const {
    PORT: port
  } = process.env;

  const httpPath = `http://localhost:${port}${apolloServer.graphqlPath}`;
  const subscriptionsPath = `ws://localhost:${port}${apolloServer.subscriptionsPath}`;

  httpServer.listen({
    port: port
  }, () => onListen(httpPath, subscriptionsPath));
}

function onListen(httpPath: string, subscriptionsPath: string) {
  logger.info('Server ready at %s', httpPath);
  logger.info('Subscriptions ready at %s', subscriptionsPath);
}

function onError(error: any) {
  logger.error(error.stack);
}
