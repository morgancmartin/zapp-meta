import winston from 'winston';

export default winston.createLogger({
  level: 'verbose',

  format: winston.format.combine(
    winston.format.splat(),
    winston.format.timestamp(),
    winston.format.printf((
      {
        timestamp: timestamp,
        level: level,
        message: message
      }
    ) => {
      return `${new Date(timestamp).toLocaleString()} [${level.toUpperCase()}] ${message}`;
    })
  ),

  transports: [new winston.transports.File({
    filename: process.env.LOGGER_FILE
  }), new winston.transports.Console({
    format: winston.format.colorize({
      all: process.env.LOGGER_COLORIZE as any
    })
  })]
});
