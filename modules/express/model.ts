import { Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export default class BaseEntity {
  id: string;
  createdAt: Date;
  updatedAt: Date;
}
