import { Module } from "~/lib/api/Module"
import { File } from '~/lib/api/Filesystem'

export default class GitModule extends Module {
  static modname = 'git'

  constructor(project: Project) {
    super(project, GitModule)
  }

  static get backend() {
    return {
      contents: [new GitIgnoreFile()]
    }
  }
  static get frontend() {
    return {
      contents: [new GitIgnoreFile()]
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]

export class GitIgnoreFile extends File {
  name = '.gitignore'

  getContents() {
    return [
      '# Created by .ignore support plugin (hsz.mobi)',
      '### Node template',
      '# Logs',
      '/logs',
      '*.log',
      'npm-debug.log*',
      'yarn-debug.log*',
      'yarn-error.log*',
      '# Runtime data',
      'pids',
      '*.pid',
      '.seed',
      '*.pid.lock',
      '# Directory for instrumented libs generated by jscoverage/JSCover',
      'lib-cov',
      '# Coverage directory used by tools like istanbul',
      'coverage',
      '# nyc test coverage',
      '.nyc_output',
      '# Grunt intermediate storage (http://gruntjs.com/creating-plugins#storing-task-files)',
      '.grunt',
      '# Bower dependency directory (https://bower.io/)',
      'bower_components',
      '# node-waf configuration',
      '.lock-wscript',
      '# Compiled binary addons (https://nodejs.org/api/addons.html)',
      'build/Release',
      '# Dependency directories',
      'node_modules/',
      'jspm_packages/',
      '# TypeScript v1 declaration files',
      'typings/',
      '# Optional npm cache directory',
      '.npm',
      '# Optional eslint cache',
      '.eslintcache',
      '# Optional REPL history',
      '.node_repl_history',
      '# Output of npm pack',
      '*.tgz',
      '# Yarn Integrity file',
      '.yarn-integrity',
      '# dotenv environment variables file',
      '.env',
      '*.env,',
      '# parcel-bundler cache (https://parceljs.org/)',
      '.cache',
      '# next.js build output',
      '.next',
      '# nuxt.js build output',
      '.nuxt',
      '# Nuxt generate',
      'dist',
      '# vuepress build output',
      '.vuepress/dist',
      '# Serverless directories',
      '.serverless',
      '# IDE / Editor',
      '.idea',
      '# Service worker',
      'sw.*',
      '# macOS',
      '.DS_Store',
      '# Vim swap files',
      '*.swp',
      '.yarn',
    ].join('\n')
  }
}
