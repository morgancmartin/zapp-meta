import { File } from '~/lib/api/Filesystem'
import { Module } from "~/lib/api/Module"

export default class CaproverModule extends Module {
  static modname = 'caprover'

  constructor(project: Project) {
    super(project, CaproverModule)
  }

  static get frontend() {
    return {
      contents: [
        new DefinitionFile(),
        new Dockerfile(),
        new FrontendDockerignore()
      ]
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]

class DefinitionFile extends File {
  name = 'captain-definition'

  getContents() {
    return '{\n  "schemaVersion": 2,\n  "dockerfilePath": "./Dockerfile"\n}'
  }
}

class Dockerfile extends File {
  name = 'Dockerfile'

  getContents() {
    return [
      'FROM node:12.18.0-alpine3.12',
      'RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app',
      'WORKDIR /home/node/app',
      'COPY package.json yarn.lock tsconfig.json ./',
      'USER node',
      'RUN yarn install',
      'COPY --chown=node:node . .',
      'ARG HOST=${HOST}',
      'ARG PORT=${PORT}',
      'ARG NODE_ENV=${NODE_ENV}',
      'ARG BASE_URL=${BASE_URL}',
      'ARG API_URL=${API_URL}',
      'ARG GRAPHQL_HTTP_URL=${GRAPHQL_HTTP_URL}',
      'ARG GRAPHQL_WS_URL=${GRAPHQL_WS_URL}',
      'RUN yarn build',
      'EXPOSE 3002',
      'CMD [ "yarn", "start" ]'
    ].join('\n')
  }
}

class FrontendDockerignore extends File {
  name = '.dockerignore'

  getContents() {
    return [
      '.dockerignore',
      'node_modules',
      'Dockerfile',
      '.git',
      '.gitignore'
    ].join('\n')
  }
}
