import { Directory } from "~/lib/api/Filesystem";
import { Module } from "~/lib/api/Module";
import { TSFile, TSFSFile } from "~/modules/typescript";
import { AnyPage } from "~/lib/api/Page";

export default class ApolloModule extends Module {
  static modname = "apollo";

  constructor(project: Project) {
    super(project, ApolloModule);
  }

  static get backend() {
    return {
      packageJson: {
        dependencies: {
          "apollo-server-express": "^2.13.1",
          graphql: "^15.0.0",
        },
      },
    };
  }

  static complete() {
    this.project.backend.withFiles([new GraphqlDir(this.project)]);
  }
}

type Project = ConstructorParameters<typeof Module>[0];

class GraphqlDir extends Directory {
  name = "graphql";
  contents = [new IndexFile(), new ScalarFile()];

  constructor(project: Project) {
    super();
    this.withFiles(
      Object.values(project.pages).map((page: AnyPage) => new GQLAPIFile(page))
    );
  }
}

export class GQLAPIFile extends TSFile {
  name: string;
  page: AnyPage;

  constructor(page: AnyPage) {
    super();
    this.name = `${page.capitalName}Page`;
    this.page = page;
  }
}

class IndexFile extends TSFSFile {
  name = "index";

  constructor() {
    super("modules/apollo/gqlindex.ts");
  }
}

class ScalarFile extends TSFSFile {
  name = "scalar";

  constructor() {
    super("modules/apollo/gqlscalar.ts");
  }
}
