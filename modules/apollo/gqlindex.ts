import { gql } from 'apollo-server-express';

export const typeDefs = gql`
  type Record {
    id: ID!
  }
`;

export const resolvers: IResolver = {};
