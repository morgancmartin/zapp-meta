import { Mixin } from 'ts-mixer'
import { File, Sourceable } from '~/lib/api/Filesystem'
import { JSONFile } from '~/lib/api/JSON'
import recast from '~/utils/recast'
import { namedTypes } from 'ast-types'
import { Serializable } from '~/utils/strings'

export class Typescriptable implements Serializable {
  get body() {
    return this.typescriptable.program.body
  }

  set body(body: namedTypes.Program['body']) {
    this.typescriptable.program.body = body
  }

  set mergeBody(body: namedTypes.Program['body']) {
    this.body = this.body.concat(body)
  }

  typescriptable = {
    b: recast.types.builders,
    program: recast.types.builders.program([])
  }

  get serializedModule() {
    const { b, program } = this.typescriptable
    return recast.prettyPrint(b.file(program))
  }

  toString() {
    return this.serializedModule
  }
}

export class TSFile extends Mixin(File, Typescriptable) {
  // @ts-ignore
  abstract name: string
  extension = 'ts'

  getContents(): string {
    return this.serializedModule
  }
}

export class TSFSFile extends Mixin(TSFile, Sourceable) {
  // @ts-ignore
  abstract name: TSFile['name']
  path: Sourceable['path']

  constructor(path: string) {
    super()
    this.path = path
    this.initFromSource()
  }

  initFromSource(): this {
    const file = recast.parse(String(this.sourceContents))
    this.typescriptable.program.body = file.program.body
    return this
  }
}

export abstract class TSConfigFile extends JSONFile {
  name = 'tsconfig'
  abstract contents: JSONFile['contents']
}
