import { Mixin } from "ts-mixer";
import { Component as ZComponent } from "~/lib/api/Component";

export class Component extends Mixin(class {}, ZComponent) {
  tailwindInput: typeof Component["tailwind"];

  static tailwind?: {
    bg?: {
      color?: ColorInput;
    };
    height?: SizeInput;
    width?: SizeInput;
  };

  constructor(
    project: Project,
    extension: typeof Component & typeof ZComponent
  ) {
    super(project, extension);
    const input = (this.tailwindInput = extension.tailwind || {});
    const bgColor = input.bg?.color;
    if (bgColor) {
      extension.withClass(getColorInputClass(bgColor));
    }
    if (input.height) {
      extension.withClass(getSizeInputClass("h", input.height));
    }
    if (input.width) {
      extension.withClass(getSizeInputClass("w", input.width));
    }
  }
}

type Project = ConstructorParameters<typeof ZComponent>[0];

type ColorInput =
  | {
      name: Color;
      weight: ColorWeight;
    }
  | SpecialColor;

type ColorWeight = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;

type SpecialColor = "transparent" | "current" | "black" | "white";

type Color =
  | "gray"
  | "red"
  | "orange"
  | "yellow"
  | "green"
  | "teal"
  | "blue"
  | "indigo"
  | "purple"
  | "pink";

function getColorInputClass(input: ColorInput) {
  if (typeof input === "string") {
    return `bg-${input}`;
  } else {
    const { name, weight } = input;
    return `bg-${name}-${weight}00`;
  }
}

type SizeWeight = 0 | ColorWeight | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17;

interface SizeInput {
  weight?: SizeWeight;
  auto?: boolean;
  px?: boolean;
  full?: boolean;
  screen?: boolean;
}

function scaleSizeWeight(weight: SizeWeight) {
  return {
    0: 0,
    1: 1,
    2: 2,
    3: 3,
    4: 4,
    5: 5,
    6: 6,
    7: 8,
    8: 10,
    9: 12,
    10: 16,
    11: 20,
    12: 24,
    13: 32,
    14: 40,
    15: 48,
    16: 56,
    17: 64,
  }[weight];
}

function getSizeInputClass(prefix: "h" | "w", input: SizeInput) {
  if (typeof input.weight === "number") {
    const scaled = scaleSizeWeight(input.weight);
    return `${prefix}-${scaled}`;
  } else if (input.auto) {
    return `${prefix}-auto`;
  } else if (input.px === true) {
    return `${prefix}-px`;
  } else if (input.full === true) {
    return `${prefix}-full`;
  } else {
    return `${prefix}-screen`;
  }
}
