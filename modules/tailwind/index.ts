import { Module } from "~/lib/api/Module"
import { Directory } from '~/lib/api/Filesystem'
import { namedTypes } from 'ast-types'
import { TSFSFile } from '~/modules/typescript'
import { NuxtContributor } from '~/modules/nuxt'
import { Mixin } from 'ts-mixer'
import { CSSFile } from '~/lib/api/CSS'

export default class Tailwind extends Mixin(Module, NuxtContributor) {
  static modname = 'tailwind'
  static project: Project

  constructor(project: Project) {
    super(project, Tailwind)
  }

  static get frontend() {
    return {
      contents: [
        new CSSDir(),
        new TailwindConfig()
      ],
      packageJson: {
        devDependencies: {
          "@tailwindcss/ui": "^0.3.0",
          "@nuxtjs/tailwindcss": "^1.0.0"
        }
      }
    }
  }

  static get nuxt() {
    return {
      extendConfig(nuxtConfig: NuxtConfig, b: Builders): ReturnType<NuxtConfigGetter> {
        return {
          visitObjectProperty(path: any) {
            const node = path.node
            if (namedTypes.Identifier.check(node.key) && node.key.name === 'buildModules') {
              node.value.elements.push(b.stringLiteral.from({ value: '@nuxtjs/tailwindcss' }))
            }
            return false
          }
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type NuxtConfigGetter = typeof NuxtContributor['nuxt']['extendConfig']
type NuxtConfigGetterParams = Parameters<NuxtConfigGetter>
type NuxtConfig = NuxtConfigGetterParams[0]
type Builders = NuxtConfigGetterParams[1]

class TailwindConfig extends TSFSFile {
  name = 'tailwind.config'

  constructor() {
    super('modules/tailwind/tailwind.config.js')
  }
}

class CSSDir extends Directory {
  name = 'css'
  contents = [
    new TailwindCSS()
  ]
}

class TailwindCSS extends CSSFile {
  name = 'tailwind'

  constructor() {
    super()
    this.withComment('purgecss start ignore')
    this.withImport(`'tailwindcss/base'`)
    this.withImport(`'tailwindcss/components'`)
    this.withComment('purgecss end ignore')
    this.withImport(`'tailwindcss/utilities'`)
  }
}
