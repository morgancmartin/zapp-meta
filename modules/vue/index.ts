import { File } from "~/lib/api/Filesystem";
import { CSSRule, CSSBlock } from "~/lib/api/CSS";
import { HTMLElement } from "~/lib/api/HTML";
import { Typescriptable } from "~/modules/typescript";
import { Mixin } from "ts-mixer";

export class VueFile extends File {
  name: string;
  extension: string = "vue";
  html: HTMLElement = HTMLElement.new("template");
  script?: ScriptTag;
  style?: StyleTag;

  constructor(name: string) {
    super();
    this.name = name;
  }

  get program() {
    return this.script?.typescriptable.program;
  }

  get b() {
    return this.script?.typescriptable.b;
  }

  withScript(): ScriptTag {
    return (this.script = new ScriptTag());
  }

  withStyle(): StyleTag {
    if (this.style) {
      return this.style;
    }
    return (this.style = new StyleTag());
  }

  getContents(): string {
    const html = this.html.toString();
    const script = this.script ? `\n${this.script.toString()}\n` : "";
    const style = this.style ? `\n${this.style.toString()}\n` : "";
    return `${html}\n${script}${style}`;
  }
}

export class ScriptTag extends Mixin(HTMLElement, Typescriptable) {
  name = "script";

  constructor() {
    super();
    this.indentLevel = 0;
    this.withAttr("lang", "ts");
    const self = this;
    this.children.push({
      toString() {
        return self.serializedModule;
      },
    });
  }

  toString(): ReturnType<typeof HTMLElement["toString"]> {
    return HTMLElement.toString(this);
  }
}

class StyleTag extends HTMLElement {
  name = "style";

  constructor() {
    super();
    this.indentLevel = 0;
    this.childSeparator = "\n\n";
  }

  withRule(property: string, value: string): this {
    this.children.push(new CSSRule([property, value]));
    return this;
  }

  withBlock(selector: string): CSSBlock {
    const block = new CSSBlock(selector);
    this.children.push(block);
    return block;
  }
}
