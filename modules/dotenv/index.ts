import { Module } from "~/lib/api/Module"
import { File } from '~/lib/api/Filesystem'
import { namedTypes } from 'ast-types'
import { NuxtContributor } from '~/modules/nuxt'
import { Mixin } from 'ts-mixer'

export default class DotenvModule extends Mixin(Module, NuxtContributor) {
  static modname = 'dotenv'

  constructor(project: Project) {
    super(project, DotenvModule)
  }

  static get frontend() {
    return {
      contents: [
        new NuxtDotenv()
      ],
      packageJson: {
        dependencies: {
          '@nuxtjs/dotenv': '^1.4.0'
        }
      }
    }
  }

  static get nuxt() {
    return {
      extendConfig(nuxtConfig: NuxtConfig, b: Builders): ReturnType<NuxtConfigGetter> {
        const dotenvConfig = b.objectProperty(
          b.identifier('dotenv'),
          b.objectExpression([b.objectProperty(b.identifier('systemvars'), b.booleanLiteral(true))])
        )
        return {
          visitObjectProperty(path: any) {
            const node = path.node
            if (namedTypes.Identifier.check(node.key) && node.key.name === 'modules') {
              node.value.elements.push(b.stringLiteral.from({ value: '@nuxtjs/dotenv' }))
            }
            return false
          },
          visitAssignmentExpression(path: any) {
            const node = path.node
            const leftIsMemberExp = namedTypes.MemberExpression.check(node.left)
            const leftObjIsModule = node.left.object.name === 'module'
            const leftPropIsExports = node.left.property.name === 'exports'
            if (leftIsMemberExp && leftObjIsModule && leftPropIsExports) {
              node.right.properties.push(dotenvConfig)
            }
            this.traverse(path)
          }
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type NuxtConfigGetter = typeof NuxtContributor['nuxt']['extendConfig']
type NuxtConfigGetterParams = Parameters<NuxtConfigGetter>
type NuxtConfig = NuxtConfigGetterParams[0]
type Builders = NuxtConfigGetterParams[1]

export class DotenvFile extends File {
  name = '.env'
  // @ts-ignore
  abstract contents: string[]

  constructor() {
    super()
  }

  getContents() {
    return this.contents.join('\n')
  }
}

class NuxtDotenv extends DotenvFile {
  contents: string[]

  constructor() {
    super()
    this.contents = [
      "HOST=localhost",
      "PORT=3002",
      "NODE_ENV=development",
      "BASE_URL=http://localhost:3002",
      "API_URL=http://localhost:3001",
      "GRAPHQL_HTTP_URL=http://localhost:3001/graphql",
      "GRAPHQL_WS_URL=ws://localhost:3001/graphql",
    ]
  }
}