// @ts-nocheck
import { Action, pipe, tryCatch } from 'overmind';

import {
  fireGqlMutation,
  awaitAndUpdateTarget,
  catchMutationError,
  transition,
  resolveGqlCreation,
  resolveGqlDeletion,
  resolveGqlUpdate,
  selectStateTarget,
  rejectGqlMutation,
} from '~/overmind/operators';

import { capitalize } from '~/utils/strings';

export function makeCreationActions(
  pageName: string,
  mutationName: string,
  collectionName: string,
  relationKeys: any[] = []
) {
  const capitalizedMutationName = capitalize(mutationName);
  const resolverName = `resolve${capitalizedMutationName}`;
  const rejecterName = `reject${capitalizedMutationName}`;

  return {
    mutationName: makeCreationAction({
      name: mutationName,
      pageName: pageName,
      resolverName: resolverName,
      rejecterName: rejecterName
    }),

    resolverName: makeCreationResolverAction(collectionName, relationKeys),
    rejecterName: rejectGqlMutation()
  };
}

export function makeDeletionActions(
  pageName: string,
  mutationName: string,
  collectionName: string,
  relationKeys: any[] = []
) {
  const capitalizedMutationName = capitalize(mutationName);
  const resolverName = `resolve${capitalizedMutationName}`;
  const rejecterName = `reject${capitalizedMutationName}`;

  return {
    mutationName: makeDeletionAction({
      name: mutationName,
      pageName: pageName,
      resolverName: resolverName,
      rejecterName: rejecterName
    }),

    resolverName: makeDeletionResolverAction(mutationName, collectionName, relationKeys),
    rejecterName: rejectGqlMutation()
  };
}

export function makeUpdateActions(
  pageName: string,
  mutationName: string,
  collectionName: string,
  relationKeys: any[] = []
) {
  const capitalizedMutationName = capitalize(mutationName);
  const resolverName = `resolve${capitalizedMutationName}`;
  const rejecterName = `reject${capitalizedMutationName}`;

  return {
    mutationName: makeUpdateAction({
      name: mutationName,
      pageName: pageName,
      resolverName: resolverName,
      rejecterName: rejecterName
    }),

    resolverName: makeUpdateResolverAction(collectionName, relationKeys),
    rejecterName: rejectGqlMutation()
  };
}

export function makeCreationAction(
  {
    name: name,
    pageName: pageName,
    resolverName: resolverName,
    rejecterName: rejecterName
  }: {
    name: string,
    pageName: string,
    resolverName: string,
    rejecterName: string
  }
) {
  const targetSelecter = selectStateTarget((state: any) => state[pageName][name]);
  const selectResolver = (actions: any) => actions[pageName][resolverName];
  const selectRejecter = (actions: any) => actions[pageName][rejecterName];

  return tryCatch({
    try: pipe(
      fireGqlMutation(name),
      targetSelecter,
      awaitAndUpdateTarget(),
      targetSelecter,
      transition(selectResolver)
    ),

    catch: pipe(catchMutationError(), targetSelecter, transition(selectRejecter))
  });
}

export function makeCreationResolverAction(collectionName: string, relationKeys: any[]) {
  return resolveGqlCreation(collectionName, relationKeys);
}

export function makeDeletionAction(
  {
    name: name,
    pageName: pageName,
    resolverName: resolverName,
    rejecterName: rejecterName
  }: {
    name: string,
    pageName: string,
    resolverName: string,
    rejecterName: string
  }
) {
  const targetSelecter = selectStateTarget((state: any) => state[pageName][name]);
  const selectResolver = (actions: any) => actions[pageName][resolverName];
  const selectRejecter = (actions: any) => actions[pageName][rejecterName];

  return tryCatch({
    try: pipe(
      fireGqlMutation(name),
      targetSelecter,
      awaitAndUpdateTarget(),
      targetSelecter,
      transition(selectResolver)
    ),

    catch: pipe(catchMutationError(), targetSelecter, transition(selectRejecter))
  });
}

export function makeDeletionResolverAction(mutationName: string, collectionName: string, relationKeys: string[]) {
  return resolveGqlDeletion(mutationName, collectionName, relationKeys);
}

export function makeUpdateAction(
  {
    name: name,
    pageName: pageName,
    resolverName: resolverName,
    rejecterName: rejecterName
  }: {
    name: string,
    pageName: string,
    resolverName: string,
    rejecterName: string
  }
) {
  const targetSelecter = selectStateTarget((state: any) => state[pageName][name]);
  const selectResolver = (actions: any) => actions[pageName][resolverName];
  const selectRejecter = (actions: any) => actions[pageName][rejecterName];

  return tryCatch({
    try: pipe(
      fireGqlMutation(name),
      targetSelecter,
      awaitAndUpdateTarget(),
      targetSelecter,
      transition(selectResolver)
    ),

    catch: pipe(catchMutationError(), targetSelecter, transition(selectRejecter))
  });
}

export function makeUpdateResolverAction(collectionName: string, relationKeys: any[]) {
  return resolveGqlUpdate(collectionName, relationKeys);
}
