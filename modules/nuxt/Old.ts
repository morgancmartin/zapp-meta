// class ButtonComponent extends Component {
//   constructor() {
//     super('Button')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     this.html
//       .withChild('span')
//       .withClass('inline-flex rounded-md shadow-sm')
//       .withChild('button')
//       .withAttr('type', 'button')
//       .withClass('inline-flex items-center px-2.5 py-1.5 border border-gray-300 text-xs leading-4 font-medium rounded text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:text-gray-800 active:bg-gray-50 transition ease-in-out duration-150')
//       .withChild('slot')
//       .withAttr('name', 'content')
//       .withText('{{ text }}')
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('Button'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('text'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('text'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Button')),
//                 b.tsLiteralType(b.stringLiteral('text'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class CheckboxComponent extends Component {
//   constructor() {
//     super('Checkbox')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const label = template.withChild('label')
//     label.withClass('inline-flex items-center')

//     const input = label.withChild('input')
//     input.withAttr(':checked', 'value')
//     input.withAttr('type', 'checkbox')
//     input.withClass('form-checkbox')
//     input.withAttr('@input', `$emit('input', $event.target.checked)`)

//     const span = label.withChild('span')
//     span.withClass('ml-2')
//     span.withText('{{ label }}')
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('Checkbox'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('label'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('value'), b.tsTypeAnnotation(b.tsBooleanKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('label'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Checkbox')),
//                 b.tsLiteralType(b.stringLiteral('label'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('value'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('Boolean')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Checkbox')),
//                 b.tsLiteralType(b.stringLiteral('value'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class SelectComponent extends Component {
//   constructor() {
//     super('Select')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const label = template.withChild('label')
//     label.withClass('block mt-4')

//     const span = label.withChild('span')
//     span.withClass('text-gray-700')
//     span.withText('{{ label }}')

//     const select = label.withChild('select')
//     select.withClass('form-select mt-1 block w-full')
//     select.withAttr(':value', 'value')
//     select.withAttr('@input', `$emit('input', $event.target.value)`)

//     const option = select.withChild('option')
//     option.withAttr('v-for', 'option in options')
//     option.withAttr(':key', 'option.text')
//     option.withAttr(':value', 'option.value')
//     option.withText('{{ option.text }}')
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('Select'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('label'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(
//           b.identifier('options'),
//           b.tsTypeAnnotation(b.tsArrayType(b.tsAnyKeyword()))
//         ),
//         b.tsPropertySignature(b.identifier('value'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('label'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Select')),
//                 b.tsLiteralType(b.stringLiteral('label'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('options'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('Array')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Select')),
//                 b.tsLiteralType(b.stringLiteral('options'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('value'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Select')),
//                 b.tsLiteralType(b.stringLiteral('value'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class TextAreaInputComponent extends Component {
//   constructor() {
//     super('TextAreaInput')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const label = template.withChild('label')
//     label.withClass('block')

//     const span = label.withChild('span')
//     span.withClass('text-gray-700')
//     span.withText('{{ label }}')

//     const textarea = label.withChild('textarea')
//     textarea.withAttr(':rows', 'rows')
//     textarea.withAttr(':placeholder', 'placeholder')
//     textarea.withAttr(':value', 'value')
//     textarea.withClass('form-input mt-1 block w-full')
//     textarea.withAttr('@input', `$emit('input', $event.target.value)`)
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('TextAreaInput'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('label'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('rows'), b.tsTypeAnnotation(b.tsNumberKeyword())),
//         b.tsPropertySignature(b.identifier('placeholder'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('value'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('label'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextAreaInput')),
//                 b.tsLiteralType(b.stringLiteral('label'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('rows'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('Number')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.numericLiteral(3))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextAreaInput')),
//                 b.tsLiteralType(b.stringLiteral('rows'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('placeholder'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextAreaInput')),
//                 b.tsLiteralType(b.stringLiteral('placeholder'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('value'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextAreaInput')),
//                 b.tsLiteralType(b.stringLiteral('value'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class TextInputComponent extends Component {
//   constructor() {
//     super('TextInput')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const label = template.withChild('label')
//     label.withClass('block')

//     const span = label.withChild('span')
//     span.withClass('text-gray-700')
//     span.withText('{{ label }}')

//     const input = label.withChild('input')
//     input.withAttr(':type', 'type')
//     input.withAttr(':placeholder', 'placeholder')
//     input.withAttr(':value', 'value')
//     input.withClass('form-input mt-1 block w-full')
//     input.withAttr('@input', `$emit('input', $event.target.value)`)
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('TextInput'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('label'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('type'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('placeholder'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('value'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('label'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextInput')),
//                 b.tsLiteralType(b.stringLiteral('label'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('type'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral('text'))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextInput')),
//                 b.tsLiteralType(b.stringLiteral('type'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('placeholder'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextInput')),
//                 b.tsLiteralType(b.stringLiteral('placeholder'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('value'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('TextInput')),
//                 b.tsLiteralType(b.stringLiteral('value'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class CardComponent extends Component {
//   constructor() {
//     super('Card')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const container = template.withChild('div')
//     container.withClass('bg-white overflow-hidden shadow rounded-lg')

//     const flex = container.withChild('div')
//     flex.withClass('flex')

//     const title = flex.withChild('div')
//     title.withClass('border-b border-gray-200 px-4 py-5 sm:px-6')

//     const titleSlot = title.withChild('slot')
//     titleSlot.withAttr('name', 'title')

//     const h3 = titleSlot.withChild('h3')
//     h3.withClass('text-lg')
//     h3.withText('{{ title }}')

//     const content = container.withChild('div')
//     content.withClass('px-4 py-5 sm:p-6')

//     const contentSlot = content.withChild('slot')
//     contentSlot.withAttr('name', 'content')
//     contentSlot.withText('Content')
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('Card'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('title'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('title'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('Card')),
//                 b.tsLiteralType(b.stringLiteral('title'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class FormComponent extends Component {
//   constructor() {
//     super('Form')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const template = this.html

//     const form = template.withChild('form')
//     form.withAttr('@submit.prevent', `$emit('submit')`)

//     const div = form.withChild('div')
//     const topContainer = div.withChild('div')
//     topContainer.withClass('mt-8 border-t border-gray-200 pt-8')

//     const titleContainer = topContainer.withChild('div')
//     const title = titleContainer.withChild('h3')
//     title.withClass('text-lg leading-6 font-medium text-gray-900')
//     title.withText('{{ title }}')

//     const contentContainer = topContainer.withChild('div')
//     contentContainer.withClass('mt-6 grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6 px-4')

//     const contentSlotContainer = contentContainer.withChild('div')
//     contentSlotContainer.withClass('sm:col-span-3')
//     const contentSlot = contentSlotContainer.withChild('slot')
//     contentSlot.withAttr('name', 'content')

//     const bottomContainer = div.withChild('div')
//     bottomContainer.withClass('mt-8 border-t border-gray-200 pt-5')
//     const buttonContainer = bottomContainer.withChild('div')
//     buttonContainer.withClass('flex justify-end')
//     const button = buttonContainer.withChild('Button')
//     button.withAttr(':text', 'btnText')
//     button.withAttr('@click', `$emit('submit')`)
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.importDeclaration(
//         [b.importDefaultSpecifier(b.identifier('Button'))],
//         b.stringLiteral('~/components/base/Button.vue')
//       ), b.tsInterfaceDeclaration(b.identifier('Form'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('title'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(b.identifier('btnText'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(b.callExpression(
//         b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false),
//         [b.objectExpression([b.objectProperty(
//           b.identifier('components'),
//           b.objectExpression([b.objectProperty(b.identifier('Button'), b.identifier('Button'))])
//         ), b.objectProperty(b.identifier('props'), b.objectExpression([
//           b.objectProperty(b.identifier('title'), b.tsAsExpression(b.objectExpression([
//             b.objectProperty(b.identifier('type'), b.identifier('String')),
//             b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//           ]), b.tsTypeReference(
//             b.identifier('PropOptions'),
//             b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//               b.tsTypeReference(b.identifier('Form')),
//               b.tsLiteralType(b.stringLiteral('title'))
//             )])
//           ))),
//           b.objectProperty(b.identifier('btnText'), b.tsAsExpression(b.objectExpression([
//             b.objectProperty(b.identifier('type'), b.identifier('String')),
//             b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//           ]), b.tsTypeReference(
//             b.identifier('PropOptions'),
//             b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//               b.tsTypeReference(b.identifier('Form')),
//               b.tsLiteralType(b.stringLiteral('btnText'))
//             )])
//           )))
//         ]))])]
//       ))]
//     }
//   }
// }

// class RadioGroupComponent extends Component {
//   constructor() {
//     super('RadioGroup')
//     this.initHTML()
//     this.initScript()
//   }

//   initHTML() {
//     const container = this.html.withChild('div')
//     container.withClass('block')

//     const title = container.withChild('span')
//     title.withClass('text-gray-700')
//     title.withText('{{ title }}')

//     const optionsContainer = container.withChild('div')
//     optionsContainer.withClass('mt-2')

//     const options = optionsContainer.withChild('div')
//     options.withAttr('v-for', '(optVal, option) in options')
//     options.withAttr('key', 'option')

//     const option = options.withChild('label')
//     option.withClass('inline-flex items-center')

//     const input = option.withChild('input')
//     input.selfClose()
//     input.withAttr(':value', 'optVal')
//     input.withAttr(':checked', 'optVal === value')
//     input.withAttr('type', 'radio')
//     input.withClass('form-radio')
//     input.withAttr('name', 'radio')
//     input.withAttr('@input', `$emit('input', $event.target.value)`)

//     const optText = option.withChild('span')
//     optText.withClass('ml-2')
//     optText.withText('{{ option }}')
//   }

//   initScript() {
//     const b = this.withScript().b
//     if (this?.program?.body) {
//       this.program.body = [b.importDeclaration([
//         b.importDefaultSpecifier(b.identifier('Vue')),
//         b.importSpecifier(b.identifier('PropOptions'), b.identifier('PropOptions'))
//       ], b.stringLiteral('vue')), b.tsInterfaceDeclaration(b.identifier('RadioGroup'), b.tsInterfaceBody([
//         b.tsPropertySignature(b.identifier('title'), b.tsTypeAnnotation(b.tsStringKeyword())),
//         b.tsPropertySignature(
//           b.identifier('options'),
//           b.tsTypeAnnotation(b.tsTypeLiteral([b.tsIndexSignature([b.identifier.from({
//             name: 'prop',
//             typeAnnotation: b.tsTypeAnnotation(b.tsStringKeyword())
//           })], b.tsTypeAnnotation(b.tsAnyKeyword()))]))
//         ),
//         b.tsPropertySignature(b.identifier('value'), b.tsTypeAnnotation(b.tsStringKeyword()))
//       ])), b.exportDefaultDeclaration(
//         b.callExpression(b.memberExpression(b.identifier('Vue'), b.identifier('extend'), false), [
//           b.objectExpression([b.objectProperty(b.identifier('props'), b.objectExpression([
//             b.objectProperty(b.identifier('title'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(false)),
//               b.objectProperty(
//                 b.identifier('default'),
//                 b.arrowFunctionExpression([], b.stringLiteral(''))
//               )
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('RadioGroup')),
//                 b.tsLiteralType(b.stringLiteral('title'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('options'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('Object')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('RadioGroup')),
//                 b.tsLiteralType(b.stringLiteral('options'))
//               )])
//             ))),
//             b.objectProperty(b.identifier('value'), b.tsAsExpression(b.objectExpression([
//               b.objectProperty(b.identifier('type'), b.identifier('String')),
//               b.objectProperty(b.identifier('required'), b.booleanLiteral(true))
//             ]), b.tsTypeReference(
//               b.identifier('PropOptions'),
//               b.tsTypeParameterInstantiation([b.tsIndexedAccessType(
//                 b.tsTypeReference(b.identifier('RadioGroup')),
//                 b.tsLiteralType(b.stringLiteral('value'))
//               )])
//             )))
//           ]))])
//         ])
//       )]
//     }
//   }
// }

// class TrashIconComponent extends Component {
//   constructor() {
//     super('TrashIcon')
//     this.initHTML()
//   }

//   initHTML() {
//     this
//       .html
//       .withChild('svg')
//       .withClass('w-4 h-4 transform scale-95 text-red-500')
//       .withAttr('fill', 'currentColor')
//       .withAttr('viewBox', '0 0 20 20')
//       .withChild('path')
//       .selfClose()
//       .withAttr('d', 'M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z')
//       .withAttr('clip-rule', 'evenodd')
//       .withAttr('fill-rule', 'evenodd')
//   }
// }
