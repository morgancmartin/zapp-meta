import { Component, ComponentFile } from "./Component";

export class PageFile extends ComponentFile {
  page: Page;

  constructor(page: Page) {
    super(page);
    this.page = page;
  }
}

export class Page extends Component {
  static nuxt?: typeof Component["nuxt"];

  constructor(project: Project, extension: typeof Page) {
    if (!extension.nuxt) {
      extension.nuxt = {
        asPage: true,
      };
    } else {
      extension.nuxt.asPage = true;
    }
    super(project, extension);
    const file = new PageFile(this);
    const pagesDir = project.frontend.findDirectory("pages");
    if (pagesDir) {
      pagesDir.withFiles([file]);
    }
  }
}

type Project = ConstructorParameters<typeof Component>[0];
