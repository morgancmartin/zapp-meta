// @ts-nocheck
import { GraphQLNormalizr } from 'graphql-normalizr';

const {
  normalize: normalize
} = new GraphQLNormalizr({
  typeMap: {}
});

export function normalizeData(data: any) {
  return normalize({
    data: data
  });
}
