import { visit, Visitor } from "ast-types";
import { Mixin } from "ts-mixer";
import { Directory } from "~/lib/api/Filesystem";
import { Module } from "~/lib/api/Module";
import { TSConfigFile, TSFSFile } from "~/modules/typescript";
export { Component, ComponentFile } from "./Component";
export { Page, PageFile } from "./Page";

export default class Nuxt extends Module {
  static modname = "nuxt";
  static project: Project;
  static config: NuxtConfig;

  constructor(project: Project) {
    super(project, Object.assign(Nuxt, { config: new NuxtConfig() }));

    if (project.frontend) {
      project.frontend.readme.withContents(getReadmeContents(project.frontend));
    }
  }

  static get frontend(): typeof Module["frontend"] {
    return {
      contents: [
        new AssetsDir(),
        new LayoutsDir(),
        new MiddlewareDir(),
        new PluginsDir(),
        new StoreDir(),
        new UtilsDir(),
        new ComponentsDir(this.project),
        new PagesDir(this.project),
        new StaticDir(this.project),
        new TSConfig(),
        this.config,
      ],
      packageJson: {
        scripts: {
          dev: "nuxt-ts",
          build: "nuxt-ts build",
          generate: "nuxt-ts generate",
          start: "nuxt-ts start",
        },
        dependencies: {
          "@nuxt/typescript-runtime": "^0.4.0",
          "@nuxtjs/pwa": "^3.0.0-0",
          lodash: "^4.17.15",
          nuxt: "^2.0.0",
        },
        devDependencies: {
          "@nuxt/typescript-build": "^0.6.0",
          "@types/lodash": "^4.14.152",
          "tsconfig-paths-webpack-plugin": "^3.2.0",
        },
      },
    };
  }
}

type Project = ConstructorParameters<typeof Module>[0];
type Frontend = Project["frontend"];

export class NuxtContributor extends Mixin(class {}, class {}) {
  static get nuxt() {
    return {
      extendConfig(
        config: NuxtConfig,
        b: NuxtConfig["typescriptable"]["b"]
      ): Visitor {
        return {};
      },
    };
  }

  constructor(project: Project, contributor: typeof NuxtContributor) {
    super();
    this.integrateNuxtConfig(project.frontend, contributor);
  }

  integrateNuxtConfig(
    frontend: Project["frontend"],
    contributor: typeof NuxtContributor
  ) {
    const config = frontend.findFile("nuxt.config");
    if (!(config instanceof NuxtConfig)) {
      return;
    }

    const { b, program } = config.typescriptable;
    visit(program, contributor.nuxt.extendConfig(config, b));
  }
}

export type ConfigMethodsGetter = typeof NuxtContributor["nuxt"]["extendConfig"];

class AssetsDir extends Directory {
  name = "assets";
}

class ComponentsDir extends Directory {
  name = "components";

  constructor(project: Project) {
    super();
  }
}

// class AppComponentsDir extends Directory {
//   name = "app";
// }

// class BaseComponentsDir extends Directory {
//   name = "base";
//   contents = [
//     // new ButtonComponent(),
//     // new CheckboxComponent(),
//     // new SelectComponent(),
//     // new TextAreaInputComponent(),
//     // new TextInputComponent()
//   ];
// }

class LayoutsDir extends Directory {
  name = "layouts";
}

class MiddlewareDir extends Directory {
  name = "middleware";
}

class PagesDir extends Directory {
  name = "pages";

  constructor(project: Project) {
    super();
  }
}

class PluginsDir extends Directory {
  name = "plugins";
}

class StaticDir extends Directory {
  name = "static";
  srcPath: string;

  constructor(project: Project) {
    super();
    this.srcPath = `/home/morgan/workspace/zapp-meta/projects/${project.name}/files`;
  }
}

class StoreDir extends Directory {
  name = "store";
}

class UtilsDir extends Directory {
  name = "utils";
  contents = [
    new StringUtilsFile(),
    new NormalizrUtilsFile(),
    new ActionsUtilsFile(),
  ];
}

// class ComplexComponentsDir extends Directory {
//   name = "complex";
//   contents = [
//     // new CardComponent(),
//     // new FormComponent(),
//     // new RadioGroupComponent()
//   ];
// }

// class IconsDir extends Directory {
//   name = "icons";
//   contents = [
//     // new TrashIconComponent()
//   ];
// }

function getReadmeContents(frontend: Frontend) {
  return [
    `# ${frontend.name}`,
    "",
    "> My scrumtrulescent Nuxt.js project",
    "",
    "## Build Setup",
    "",
    "```bash",
    "# install dependencies",
    "$ yarn install",
    "",
    "# serve with hot reload at localhost:3000",
    "$ yarn dev",
    "",
    "# build for production and launch server",
    "$ yarn build",
    "$ yarn start",
    "",
    "# generate static project",
    "$ yarn generate",
    "```",
    "",
    "For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).",
  ];
}

export class NuxtConfig extends TSFSFile {
  name = "nuxt.config";

  constructor() {
    super("modules/nuxt/nuxt.config.ts");
  }
}

class StringUtilsFile extends TSFSFile {
  name = "strings";

  constructor() {
    super("modules/nuxt/strings.ts");
  }
}

class NormalizrUtilsFile extends TSFSFile {
  name = "normalizr";

  constructor() {
    super("modules/nuxt/normalizr.ts");
  }
}

class ActionsUtilsFile extends TSFSFile {
  name = "actions";

  constructor() {
    super("modules/nuxt/actions.ts");
  }
}

class TSConfig extends TSConfigFile {
  contents = {
    compilerOptions: {
      target: "es2018",
      module: "esnext",
      // "module": "commonjs",
      moduleResolution: "node",
      lib: ["esnext", "esnext.asynciterable", "dom"],
      esModuleInterop: true,
      allowJs: true,
      sourceMap: true,
      strict: true,
      noEmit: true,
      /* Enables experimental support for ES7 decorators. */
      experimentalDecorators: true,
      /* Enables experimental support for emitting type metadata for decorators. */
      // "emitDecoratorMetadata": true,
      baseUrl: ".",
      paths: {
        "~/*": ["./*"],
        "@/*": ["./*"],
        "@GQL/*": ["./overmind/effects/gql/graphql-types/*"],
        "@GQL": ["./overmind/graphql-global-types"],
      },
      types: ["@types/node", "@nuxt/types"],
    },
    exclude: ["node_modules", ".nuxt", "dist"],
  };
}
