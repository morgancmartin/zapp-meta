import { Mixin } from "ts-mixer";
import {
  Component as ZComponent,
  AnyComponentInput as AnyZCmpInput,
  Attribute as ZAttr,
  Prop as ZCmpProp,
  AnyPropRef,
} from "~/lib/api/Component";
import { VueFile } from "~/modules/vue";
import { HTMLElement } from "~/lib/api/HTML";

export class Component extends Mixin(ZComponent) {
  nuxtInput: typeof Component["nuxt"];

  static nuxt?: {
    elementName?: string;
    asPage?: boolean;
  };

  constructor(project: Project, extension: typeof Component) {
    super(project, extension);
    this.nuxtInput = extension.nuxt || {};
    if (!this.nuxtInput.asPage) {
      const file = new ComponentFile(this);
      const componentsDir = project.frontend.findDirectory("components");
      if (componentsDir) {
        componentsDir.withFiles([file]);
      }
    }
  }
}

type Project = ConstructorParameters<typeof ZComponent>[0];

export class ComponentFile extends VueFile {
  component: Component;
  script: Required<VueFile>["script"];

  get b() {
    return this.script.typescriptable.b;
  }

  constructor(component: Component) {
    super(component.zapp.name);
    this.component = component;
    this.script = this.withScript();
    this.buildBody();
    this.buildHtml();
  }

  buildBody() {
    this.script.body = this.getBody();
  }

  buildHtml() {
    const { component } = this;
    this.html.children = [];
    const input = component.nuxtInput;
    const container = this.html.withChild("div");
    let attrTarget = container;
    component.zapp.container?.attributes?.forEach(
      (attr: [string, string | AnyPropRef]) => {
        attrTarget.withAttr(
          attr[0],
          typeof attr[1] === "string" ? attr[1] : attr[1]?.propName
        );
      }
    );
    if (input?.elementName) {
      attrTarget = container.withChild(input.elementName);
    }
    component.attributes.forEach((attr: [string, string | AnyPropRef]) => {
      attrTarget.withAttr(
        attr[0],
        typeof attr[1] === "string" ? attr[1] : attr[1]?.propName
      );
    });
    component.components.forEach((cmpInput: AnyZCmpInput) => {
      this.attachCmpInput(attrTarget, cmpInput);
    });
    if (typeof component.zapp.text === "string") {
      attrTarget.withText(component.zapp.text);
    } else if (component.zapp.text) {
      attrTarget.withText(`{{ text }}`);
    }
  }

  attachCmpInput(parent: HTMLElement, cmpInput: AnyZCmpInput) {
    const [childCmp, attrs] = cmpInput;
    const elementOnly = childCmp.zapp?.elementOnly;
    const name = elementOnly ?? childCmp.zapp.name;
    const childEl = parent.withChild(name);
    attrs.forEach((attr: ZAttr) => {
      if (typeof attr[1] === "string") {
        childEl.withAttr(attr[0], attr[1]);
      } else {
        const propRef: AnyPropRef = attr[1];
        childEl.withAttr(`:${attr[0]}`, propRef.propName);
      }
    });
    const childInputs = cmpInput[2];
    if (childInputs) {
      childInputs.forEach((childInput: AnyZCmpInput) => {
        this.attachCmpInput(childEl, childInput);
      });
    }
  }

  getBody() {
    // const b = this.b;
    return [...this.imports, this.interface, this.export];
  }

  get imports() {
    const b = this.b;
    return [
      b.importDeclaration(
        [
          b.importDefaultSpecifier(b.identifier("Vue")),
          b.importSpecifier(
            b.identifier("PropOptions"),
            b.identifier("PropOptions")
          ),
        ],
        b.stringLiteral("vue")
      ),
      ...this.componentImports,
    ];
  }

  get componentImports() {
    const { b } = this;
    return (
      this.component.uniqComponentClasses.map((Cmp: typeof ZComponent) => {
        return b.importDeclaration(
          [b.importDefaultSpecifier(b.identifier(Cmp.zapp.name))],
          b.stringLiteral(`~/components/${Cmp.zapp.name}.vue`)
        );
      }) ?? []
    );
  }

  get interface() {
    const b = this.b;
    return b.tsInterfaceDeclaration(
      b.identifier(this.component.capitalName),
      b.tsInterfaceBody(this.interfaceProps)
    );
  }

  get interfaceProps() {
    const { b } = this;
    return (
      this.component.zapp.props.map(
        (prop: ZCmpProp<string, "string" | "boolean">) =>
          this.b.tsPropertySignature(
            b.identifier(prop.name),
            b.tsTypeAnnotation(b.tsTypeReference(b.identifier(prop.type)))
          )
      ) ?? []
    );
  }

  get export() {
    const b = this.b;
    return b.exportDefaultDeclaration(
      b.callExpression(
        b.memberExpression(b.identifier("Vue"), b.identifier("extend"), false),
        [
          b.objectExpression([
            ...this.components,
            ...this.props,
            ...this.data,
            ...this.methods,
          ]),
        ]
      )
    );
  }

  get components() {
    const b = this.b;
    const props = this.component.uniqComponentClasses.map(
      (Cmp: typeof ZComponent) =>
        b.objectProperty(
          b.identifier(Cmp.zapp.name),
          b.identifier(Cmp.zapp.name)
        )
    );
    return props.length
      ? [
          b.objectProperty(
            b.identifier("components"),
            b.objectExpression(props)
          ),
        ]
      : [];
  }

  get props() {
    const b = this.b;
    const isRequired = (prop: ZCmpProp<string, "string" | "boolean">) =>
      prop.required ?? true;
    const defaultProp = (prop: ZCmpProp<string, "string" | "boolean">) =>
      isRequired(prop)
        ? []
        : [
            b.objectProperty(
              b.identifier("default"),
              b.arrowFunctionExpression([], b.stringLiteral(""))
            ),
          ];
    const props = this.component.zapp.props.map(
      (prop: ZCmpProp<string, "string" | "boolean">) =>
        b.objectProperty(
          b.identifier(prop.name),
          b.tsAsExpression(
            b.objectExpression([
              b.objectProperty(
                b.identifier("type"),
                b.identifier(
                  {
                    string: "String",
                    boolean: "Boolean",
                  }[prop.type]
                )
              ),
              b.objectProperty(
                b.identifier("required"),
                b.booleanLiteral(isRequired(prop))
              ),
              ...defaultProp(prop),
            ]),
            b.tsTypeReference(
              b.identifier("PropOptions"),
              b.tsTypeParameterInstantiation([
                b.tsIndexedAccessType(
                  b.tsTypeReference(b.identifier(this.component.capitalName)),
                  b.tsLiteralType(b.stringLiteral(prop.name))
                ),
              ])
            )
          )
        )
    );
    return props.length
      ? [b.objectProperty(b.identifier("props"), b.objectExpression(props))]
      : [];
  }

  get data() {
    // const b = this.b;
    return [];
    // return b.objectMethod(
    //   "method",
    //   b.identifier("data"),
    //   [
    //     b.identifier.from({
    //       name: "self",
    //       typeAnnotation: b.tsTypeAnnotation(b.tsAnyKeyword()),
    //     }),
    //   ],
    //   b.blockStatement([
    //     b.returnStatement(
    //       b.objectExpression([
    //         b.objectProperty(
    //           b.identifier("destinationId"),
    //           b.memberExpression(
    //             b.identifier("self"),
    //             b.identifier("initialDestinationId"),
    //             false
    //           )
    //         ),
    //         b.objectMethod(
    //           "get",
    //           b.identifier("destinationOpts"),
    //           [],
    //           b.blockStatement([
    //             b.returnStatement(
    //               b.callExpression(
    //                 b.memberExpression(
    //                   b.memberExpression(
    //                     b.memberExpression(
    //                       b.identifier("self"),
    //                       b.identifier("state"),
    //                       false
    //                     ),
    //                     b.identifier("destinationList"),
    //                     false
    //                   ),
    //                   b.identifier("map"),
    //                   false
    //                 ),
    //                 [
    //                   b.arrowFunctionExpression(
    //                     [
    //                       b.identifier.from({
    //                         name: "destination",
    //                         typeAnnotation: b.tsTypeAnnotation(
    //                           b.tsAnyKeyword()
    //                         ),
    //                       }),
    //                     ],
    //                     b.objectExpression([
    //                       b.objectProperty(
    //                         b.identifier("text"),
    //                         b.memberExpression(
    //                           b.identifier("destination"),
    //                           b.identifier("name"),
    //                           false
    //                         )
    //                       ),
    //                       b.objectProperty(
    //                         b.identifier("value"),
    //                         b.memberExpression(
    //                           b.identifier("destination"),
    //                           b.identifier("id"),
    //                           false
    //                         )
    //                       ),
    //                     ])
    //                   ),
    //                 ]
    //               )
    //             ),
    //           ]),
    //           false
    //         ),
    //       ])
    //     ),
    //   ]),
    //   false
    // );
  }

  get methods() {
    // const b = this.b;
    return [];
    // return b.objectProperty(
    //   b.identifier("methods"),
    //   b.objectExpression([
    //     b.objectMethod(
    //       "method",
    //       b.identifier("updateDestinationId"),
    //       [
    //         b.identifier.from({
    //           name: "destinationId",
    //           typeAnnotation: b.tsTypeAnnotation(b.tsStringKeyword()),
    //         }),
    //       ],
    //       b.blockStatement([
    //         b.variableDeclaration("const", [
    //           b.variableDeclarator(
    //             b.identifier.from({
    //               name: "self",
    //               typeAnnotation: b.tsTypeAnnotation(b.tsAnyKeyword()),
    //             }),
    //             b.thisExpression()
    //           ),
    //         ]),
    //         b.expressionStatement(
    //           b.assignmentExpression(
    //             "=",
    //             b.memberExpression(
    //               b.identifier("self"),
    //               b.identifier("destinationId"),
    //               false
    //             ),
    //             b.identifier("destinationId")
    //           )
    //         ),
    //         b.expressionStatement(
    //           b.callExpression(
    //             b.memberExpression(
    //               b.identifier("self"),
    //               b.identifier("$emit"),
    //               false
    //             ),
    //             [
    //               b.stringLiteral("destinationUpdated"),
    //               b.identifier("destinationId"),
    //             ]
    //           )
    //         ),
    //       ]),
    //       false
    //     ),
    //   ])
    // );
  }
}
