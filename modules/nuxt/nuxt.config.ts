// @ts-nocheck
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'

module.exports = {
  mode: 'universal',

  head: {
    title: process.env.npm_package_name || '',

    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }, {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || ''
    }],

    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }, {
      rel: 'stylesheet',
      href: 'https://rsms.me/inter/inter.css'
    }]
  },

  loading: {
    color: '#fff'
  },

  css: [],
  plugins: [],

  server: {
    host: process.env.HOST,
    port: process.env.PORT
  },

  buildModules: ['@nuxt/typescript-build'],
  modules: ['@nuxtjs/pwa'],

  build: {
    extend(config: any) {
      if (!config.resolve.plugins) {
        config.resolve.plugins = []
      }

      config.resolve.plugins.push(new TsconfigPathsPlugin({
        configFile: './tsconfig.json'
      }))
    }
  }
}
