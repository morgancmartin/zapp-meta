import { namedTypes } from 'ast-types'
import { Mixin } from 'ts-mixer'
import { Module } from "~/lib/api/Module"
import { NuxtContributor } from '~/modules/nuxt'

export default class Axios extends Mixin(Module, NuxtContributor) {
  static modname = 'axios'

  constructor(project: Project) {
    super(project, Axios)
  }

  static get frontend() {
    return {
      packageJson: {
        dependencies: {
          "@nuxtjs/axios": "^5.3.6",
        }
      }
    }
  }

  static get nuxt() {
    return {
      extendConfig(nuxtConfig: NuxtConfig, b: Builders): ReturnType<NuxtConfigGetter> {
        return {
          visitObjectProperty(path: any) {
            const node = path.node
            if (namedTypes.Identifier.check(node.key) && node.key.name === 'modules') {
              node.value.elements.push(b.stringLiteral.from({ value: '@nuxtjs/axios' }))
            }
            return false
          }
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type NuxtConfigGetter = typeof NuxtContributor['nuxt']['extendConfig']
type NuxtConfigGetterParams = Parameters<NuxtConfigGetter>
type NuxtConfig = NuxtConfigGetterParams[0]
type Builders = NuxtConfigGetterParams[1]
