const {
  exec: exec
} = require('child_process');

module.exports = {
  name: `apollo-gen-schema`,

  factory: require => {
    const {
      Command: Command
    } = require(`clipanion`);

    class HelloWorldCommand extends Command {
      async execute() {
        const cmd = [
          'yarn',
          'apollo codegen:generate',
          '--endpoint=http://localhost:3001/graphql',
          '--target=typescript',
          '--includes=overmind/**/*.ts',
          '--tagName=gql',
          '--no-addTypename',
          '--globalTypesFile=overmind/graphql-global-types.ts',
          'graphql-types'
        ].join(' ');

        exec(cmd, (error, stdout, stderr) => {
          if (error || stderr) {
            this.context.stderr.write(String(error || stderr));
            return;
          }

          this.context.stdout.write(stdout);
        });
      }
    }

    HelloWorldCommand.addPath(`schema`);

    HelloWorldCommand.usage = Command.Usage({
      description: `generate apollo schema because yarn 2.0's package script running is jacked up`,
      details: `calls the following directly: yarn apollo codegen:generate --endpoint=http://localhost:3001/graphql --target=typescript --includes=overmind/**/*.ts --tagName=gql --no-addTypename --globalTypesFile=overmind/graphql-global-types.ts graphql-types`,
      examples: [[`use it just like so:`, `yarn schema`]]
    });

    return {
      commands: [HelloWorldCommand]
    };
  }
};
