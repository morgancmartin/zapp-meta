import { Module } from "~/lib/api/Module"
import { Directory, File, FSFile } from '~/lib/api/Filesystem'
import { TSFSFile } from '~/modules/typescript'

export default class YarnModule extends Module {
  static modname = 'yarn'

  constructor(project: Project) {
    super(project, YarnModule)
  }

  static get backend() {
    return {
      contents: [
        new Plugins(),
        new DotYarnDir(),
        new YarnrcYml()
      ]
    }
  }

  static get frontend() {
    return {
      contents: [
        new Plugins(),
        new DotYarnDir(),
        new YarnrcYml()
      ]
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]

class YarnrcYml extends File {
  name = '.yarnrc.yml'

  getContents() {
    return [
      'yarnPath: ".yarn/releases/yarn-berry.js"',
      'nodeLinker: node-modules',
      'plugins:',
      '  - ./yarnplugins/schema.js'
    ].join('\n')
  }
}

class DotYarnDir extends Directory {
  name = '.yarn'
  contents = [
    new ReleasesDir()
  ]
}

class ReleasesDir extends Directory {
  name = 'releases'
  contents = [
    new YarnBerry()
  ]
}

class YarnBerry extends FSFile {
  name = 'yarn-berry.js'
  path = 'modules/yarn/yarn-berry.js'
}

class Plugins extends Directory {
  name = 'yarnplugins'
  contents = [
    new ApolloSchemaPlugin()
  ]
}

class ApolloSchemaPlugin extends TSFSFile {
  name = 'schema'
  extension = 'js'

  constructor() {
    super('modules/yarn/apolloschemaplugin.js')
  }
}
