import { Module } from "~/lib/api/Module"
import { namedTypes } from 'ast-types'
import { TSFSFile } from '~/modules/typescript'
import { NuxtContributor } from '~/modules/nuxt'
import { Mixin } from 'ts-mixer'

export default class ESLint extends Mixin(Module, NuxtContributor) {
  static modname = 'eslint'

  constructor(project: Project) {
    super(project, ESLint)
  }

  static get frontend() {
    return {
      contents: [
        new EslintRc()
      ],
      packageJson: {
        scripts: {
          'lint': 'eslint --ext .js,.vue --ignore-path .gitignore .'
        },
        'lint-staged': {
          '*.{js,vue}': 'yarn lint'
        },
        'husky': {
          'hooks': {
            'pre-commit': 'lint-staged'
          }
        },
        devdependencies: {
          '@nuxtjs/eslint-config-typescript': '^1.0.0',
          '@nuxtjs/eslint-module': '^1.0.0',
          'babel-eslint': '^10.0.1',
          'eslint': '^6.1.0',
          'eslint-plugin-nuxt': '>=0.4.2',
          'husky': '^4.0.0',
          'lint-staged': '^10.0.0'
        }
      }
    }
  }

  static get nuxt() {
    return {
      extendConfig(nuxtConfig: NuxtConfig, b: Builders): ReturnType<NuxtConfigGetter> {
        const typescriptConfig = b.objectProperty(
          b.identifier('typescript'),
          b.objectExpression([b.objectProperty(b.identifier('typecheck'), b.objectExpression([
            b.objectProperty(b.identifier('eslint'), b.booleanLiteral(true)),
            b.objectProperty(
              b.identifier('eslintOptions'),
              b.objectExpression([b.objectProperty(b.identifier('fix'), b.booleanLiteral(true))])
            )
          ]))])
        )
  
        return {
          visitAssignmentExpression(path: any) {
            const node = path.node
            const leftIsMemberExp = namedTypes.MemberExpression.check(node.left)
            const leftObjIsModule = node.left.object.name === 'module'
            const leftPropIsExports = node.left.property.name === 'exports'
            if (leftIsMemberExp && leftObjIsModule && leftPropIsExports) {
              node.right.properties.push(typescriptConfig)
            }
            return false
          }
        }
      }
    }
  }
}

type Project = ConstructorParameters<typeof Module>[0]
type NuxtConfigGetter = typeof NuxtContributor['nuxt']['extendConfig']
type NuxtConfigGetterParams = Parameters<NuxtConfigGetter>
type NuxtConfig = NuxtConfigGetterParams[0]
type Builders = NuxtConfigGetterParams[1]

class EslintRc extends TSFSFile {
  name = '.eslintrc'
  extension = 'js'

  constructor() {
    super('modules/eslint/frontendeslintrc.js')
  }
}
