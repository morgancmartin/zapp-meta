import * as fs from 'fs'
import * as util from 'util'
import * as childProcess from 'child_process'

const exec = util.promisify(childProcess.exec)

export async function readFile(path: string): Promise<{ err: Error | null, data: any }> {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', async (err, data) => {
      resolve({ err, data })
    })
  })
}

export async function bash(cmd: string) {
  const { stdout, stderr } = await exec(cmd)
  if (stderr) {
    console.log(stderr)
    return false
  } else if (stdout) {
    return true
  } else {
    return true
  }
}
