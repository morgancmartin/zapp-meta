export function objToHtmlSource(obj: any, indent = 0) {
  const indention = Array(indent).fill(" ").join("");

  if (obj.text) {
    return `${indention}${obj.text.split("\n").join("\n" + indention)}`;
  }

  const { name } = obj;

  const attrObjs = obj.attributes || [];
  const sourceAttrs = attrObjs.map(
    (attr: any) => `${attr.name}="${attr.value}"`
  );
  const splitAttrs = sourceAttrs.join(" ").length > 50;

  const attrJoiner = splitAttrs ? `\n${indention}  ` : " ";

  const attributes = !attrObjs.length
    ? ""
    : attrJoiner +
      sourceAttrs.join(attrJoiner) +
      (attrJoiner === " " ? "" : `\n${indention.slice()}`);

  const children = !obj.children
    ? ""
    : indention +
      obj.children
        .map((childObj: any) => {
          return objToHtmlSource(
            childObj,
            childObj.text && name === "script" ? 0 : 2
          )
            .split("\n")
            .join(`\n${indention}`);
        })
        .join(`\n${indention}`);

  const selfClosing = !children;

  if (selfClosing) {
    return `${indention}<${name}${attributes}${splitAttrs ? "" : " "}/>`;
  } else {
    return `${indention}<${name}${attributes}>\n${children}\n${indention}</${name}>`;
  }
}
