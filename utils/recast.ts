import * as recast from 'recast'
import * as parser from 'recast/parsers/typescript'
import { visit } from 'ast-types'
import { every } from 'lodash'
import { typeToBuild } from './recastTypeToBuild'

const b = recast.types.builders

export default {
  ...recast,
  prettyPrint,
  parse,
  sourceToMeta,
  metaToSource,
  sourceToMetaToSource
}

function prettyPrint(ast: any) {
  return recast.prettyPrint(ast, { quote: 'single', tabWidth: 2 }).code
}

function parse(source: string) {
  return recast.parse(source, { parser })
}

function sourceToMeta(source: string) {
  const ast = parse(source)
  const meta = parse(nodeToMeta(ast))
  return meta
}

function metaToSource(meta: any) {
  visit(meta, {
    visitRegExpLiteral(path: any) {
      const node = path.value
      if (!node.extra) {
        const raw = `/${node.pattern}/${node.flags || ''}`
        node.extra = { raw }
      }
      return false
    }
  })
  return prettyPrint(meta)
}

function sourceToMetaToSource(source: string) {
  return metaToSource(eval(prettyPrint(sourceToMeta(source))))
}

function nodeToMeta(node: any): string {
  const builderFnName = lowercaseFirstChar(node.type)

  const buildFieldNames = typeToBuild[node.type].build
  const fieldNames = recast.types.getFieldNames(node)
  const hasAllBuildFields = every(buildFieldNames, (fieldName: string) => fieldNames.includes(fieldName))
  const specialFieldNames = ['decorators', 'typeAnnotation', 'async', 'declare']
  const hasSpecialField = (fieldName: string) =>
    !buildFieldNames.includes(fieldName) &&
    fieldNames.includes(fieldName) &&
    Boolean(node[fieldName])

  const isDecorator = node.type === 'Decorator'

  const additionalArgs = specialFieldNames.filter(hasSpecialField)
  const hasAdditionalArgs = Boolean(additionalArgs.length)

  const asListArgs = isDecorator || (hasAllBuildFields && !hasAdditionalArgs)
  const argFields = [...buildFieldNames].concat(additionalArgs)

  const args = argFields
    .filter((field: string) => node[field] !== undefined && field !== 'type')
    .map((field: string) => {
      const value = getMetaNodeFieldValue(node[field])
      return asListArgs ? value : `${field}: ${value}`
    }).join(', ')

  return asListArgs
    ? `b.${builderFnName}(${args})`
    : `b.${builderFnName}.from({ ${args} })`
}

function getMetaNodeFieldValue(value: any | any[]) {
  if (isRecastNode(value)) {
    return nodeToMeta(value)
  } else if (Array.isArray(value)) {
    const values = value
      .map((value: any) => isRecastNode(value) ? nodeToMeta(value) : value)
      .join(', ')
    return `[${values}]`
  } else {
    return JSON.stringify(value)
  }
}

function isRecastNode(value: any) {
  if (!value) { return false }
  const types: any = recast.types
  const namedType: any = types.namedTypes[value.type]
  return namedType && namedType.predicate(value)
}

function lowercaseFirstChar(str: string) {
  if (str.slice(0, 2) === 'TS') {
    return `${str.slice(0, 2).toLowerCase()}${str.slice(2, str.length)}`
  } else {
    return `${str.slice(0, 1).toLowerCase()}${str.slice(1, str.length)}`
  }
}

export function objectToMeta(obj: any) {
  const props: any[] = Object.entries(obj).map(([key, value]: [string, any]) => {
    const builder = b[`${typeof value}Literal`]
    return builder ? b.objectProperty(b.identifier(key), builder(value)) : null
  }).filter(Boolean)

  return recast.types.builders.objectExpression(props)
}
