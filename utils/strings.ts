import pluralize from 'pluralize'

export function capitalize (str: string){
  const chars = str.split('')
  chars[0] = chars[0].toUpperCase()
  return chars.join('')
}

export function getIndention(numIndent: number) {
  return Array(numIndent).fill(' ').join('')
}

export function indentLines(lines: string, numIndent = 2): string {
  const indention = getIndention(numIndent)
  const indent = (i: string): string => i.trim().length ? `${indention}${i}` : i
  return lines.split('\n').map(indent).join('\n')
}

export interface Serializable {
  toString(): string
}

export class Named {
  // @ts-ignore
  abstract name: string

  static get pluralName() {
    return pluralize(this.name)
  }

  get pluralName() {
    return pluralize(this.name)
  }

  static get capitalName() {
    return this.name.split(' ').map(capitalize).join(' ')
  }

  get capitalName() {
    return this.name.split(' ').map(capitalize).join(' ')
  }

  static get pluralCapitalName() {
    return pluralize(this.capitalName)
  }

  get pluralCapitalName() {
    return pluralize(this.capitalName)
  }
}