export const typeToBuild: any = {
  "Statement": {
    "build": []
  },
  "Noop": {
    "build": []
  },
  "Expression": {
    "build": []
  },
  "DoExpression": {
    "build": [
      "body"
    ]
  },
  "Super": {
    "build": []
  },
  "BindExpression": {
    "build": [
      "object",
      "callee"
    ]
  },
  "Node": {
    "build": []
  },
  "Decorator": {
    "build": [
      "callee"
    ]
  },
  "Property": {
    "build": [
      "kind",
      "key",
      "value"
    ]
  },
  "MethodDefinition": {
    "build": [
      "kind",
      "key",
      "value",
      "static"
    ]
  },
  "MetaProperty": {
    "build": [
      "meta",
      "property"
    ]
  },
  "Identifier": {
    "build": [
      "name"
    ]
  },
  "ParenthesizedExpression": {
    "build": [
      "expression"
    ]
  },
  "ModuleSpecifier": {
    "build": []
  },
  "ImportSpecifier": {
    "build": [
      "imported",
      "local",
      "id",
      "name"
    ]
  },
  "ImportDefaultSpecifier": {
    "build": [
      "local",
      "id"
    ]
  },
  "ImportNamespaceSpecifier": {
    "build": [
      "local",
      "id"
    ]
  },
  "Declaration": {
    "build": []
  },
  "ExportDefaultDeclaration": {
    "build": [
      "declaration"
    ]
  },
  "ExportNamedDeclaration": {
    "build": [
      "declaration",
      "specifiers",
      "source"
    ]
  },
  "ExportSpecifier": {
    "build": [
      "local",
      "exported",
      "id",
      "name"
    ]
  },
  "Literal": {
    "build": [
      "value"
    ]
  },
  "Specifier": {
    "build": []
  },
  "ExportNamespaceSpecifier": {
    "build": [
      "exported"
    ]
  },
  "ExportDefaultSpecifier": {
    "build": [
      "exported"
    ]
  },
  "ExportAllDeclaration": {
    "build": [
      "exported",
      "source"
    ]
  },
  "Comment": {
    "build": []
  },
  "CommentBlock": {
    "build": [
      "value",
      "leading",
      "trailing"
    ]
  },
  "CommentLine": {
    "build": [
      "value",
      "leading",
      "trailing"
    ]
  },
  "Directive": {
    "build": [
      "value"
    ]
  },
  "DirectiveLiteral": {
    "build": [
      "value"
    ]
  },
  "InterpreterDirective": {
    "build": [
      "value"
    ]
  },
  "BlockStatement": {
    "build": [
      "body"
    ]
  },
  "Program": {
    "build": [
      "body"
    ]
  },
  "StringLiteral": {
    "build": [
      "value"
    ]
  },
  "NumericLiteral": {
    "build": [
      "value"
    ]
  },
  "BigIntLiteral": {
    "build": [
      "value"
    ]
  },
  "NullLiteral": {
    "build": []
  },
  "BooleanLiteral": {
    "build": [
      "value"
    ]
  },
  "RegExpLiteral": {
    "build": [
      "pattern",
      "flags"
    ]
  },
  "ObjectExpression": {
    "build": [
      "properties"
    ]
  },
  "Function": {
    "build": []
  },
  "ObjectMethod": {
    "build": [
      "kind",
      "key",
      "params",
      "body",
      "computed"
    ]
  },
  "Pattern": {
    "build": []
  },
  "ObjectProperty": {
    "build": [
      "key",
      "value"
    ]
  },
  "ClassBody": {
    "build": [
      "body"
    ]
  },
  "ClassMethod": {
    "build": [
      "kind",
      "key",
      "params",
      "body",
      "computed",
      "static"
    ]
  },
  "ClassPrivateMethod": {
    "build": [
      "key",
      "params",
      "body",
      "kind",
      "computed",
      "static"
    ]
  },
  "PrivateName": {
    "build": [
      "id"
    ]
  },
  "ClassProperty": {
    "build": [
      "key",
      "value",
      "typeAnnotation",
      "static"
    ]
  },
  "ClassPrivateProperty": {
    "build": [
      "key",
      "value"
    ]
  },
  "ObjectPattern": {
    "build": [
      "properties"
    ]
  },
  "SpreadProperty": {
    "build": [
      "argument"
    ]
  },
  "RestProperty": {
    "build": [
      "argument"
    ]
  },
  "ForAwaitStatement": {
    "build": [
      "left",
      "right",
      "body"
    ]
  },
  "VariableDeclaration": {
    "build": [
      "kind",
      "declarations"
    ]
  },
  "Import": {
    "build": []
  },
  "Printable": {
    "build": []
  },
  "SourceLocation": {
    "build": []
  },
  "Position": {
    "build": []
  },
  "File": {
    "build": [
      "program",
      "name"
    ]
  },
  "EmptyStatement": {
    "build": []
  },
  "ExpressionStatement": {
    "build": [
      "expression"
    ]
  },
  "IfStatement": {
    "build": [
      "test",
      "consequent",
      "alternate"
    ]
  },
  "LabeledStatement": {
    "build": [
      "label",
      "body"
    ]
  },
  "BreakStatement": {
    "build": [
      "label"
    ]
  },
  "ContinueStatement": {
    "build": [
      "label"
    ]
  },
  "WithStatement": {
    "build": [
      "object",
      "body"
    ]
  },
  "SwitchStatement": {
    "build": [
      "discriminant",
      "cases",
      "lexical"
    ]
  },
  "SwitchCase": {
    "build": [
      "test",
      "consequent"
    ]
  },
  "ReturnStatement": {
    "build": [
      "argument"
    ]
  },
  "ThrowStatement": {
    "build": [
      "argument"
    ]
  },
  "TryStatement": {
    "build": [
      "block",
      "handler",
      "finalizer"
    ]
  },
  "CatchClause": {
    "build": [
      "param",
      "guard",
      "body"
    ]
  },
  "WhileStatement": {
    "build": [
      "test",
      "body"
    ]
  },
  "DoWhileStatement": {
    "build": [
      "body",
      "test"
    ]
  },
  "ForStatement": {
    "build": [
      "init",
      "test",
      "update",
      "body"
    ]
  },
  "ForInStatement": {
    "build": [
      "left",
      "right",
      "body"
    ]
  },
  "DebuggerStatement": {
    "build": []
  },
  "FunctionDeclaration": {
    "build": [
      "id",
      "params",
      "body",
      "generator",
      "expression"
    ]
  },
  "FunctionExpression": {
    "build": [
      "id",
      "params",
      "body",
      "generator",
      "expression"
    ]
  },
  "VariableDeclarator": {
    "build": [
      "id",
      "init"
    ]
  },
  "ThisExpression": {
    "build": []
  },
  "ArrayExpression": {
    "build": [
      "elements"
    ]
  },
  "SequenceExpression": {
    "build": [
      "expressions"
    ]
  },
  "UnaryExpression": {
    "build": [
      "operator",
      "argument",
      "prefix"
    ]
  },
  "BinaryExpression": {
    "build": [
      "operator",
      "left",
      "right"
    ]
  },
  "AssignmentExpression": {
    "build": [
      "operator",
      "left",
      "right"
    ]
  },
  "MemberExpression": {
    "build": [
      "object",
      "property",
      "computed"
    ]
  },
  "UpdateExpression": {
    "build": [
      "operator",
      "argument",
      "prefix"
    ]
  },
  "LogicalExpression": {
    "build": [
      "operator",
      "left",
      "right"
    ]
  },
  "ConditionalExpression": {
    "build": [
      "test",
      "consequent",
      "alternate"
    ]
  },
  "NewExpression": {
    "build": [
      "callee",
      "arguments"
    ]
  },
  "CallExpression": {
    "build": [
      "callee",
      "arguments"
    ]
  },
  "OptionalMemberExpression": {
    "build": [
      "object",
      "property",
      "computed",
      "optional"
    ]
  },
  "OptionalCallExpression": {
    "build": [
      "callee",
      "arguments",
      "optional"
    ]
  },
  "RestElement": {
    "build": [
      "argument"
    ]
  },
  "TypeAnnotation": {
    "build": [
      "typeAnnotation"
    ]
  },
  "TSTypeAnnotation": {
    "build": [
      "typeAnnotation"
    ]
  },
  "SpreadElementPattern": {
    "build": [
      "argument"
    ]
  },
  "ArrowFunctionExpression": {
    "build": [
      "params",
      "body",
      "expression"
    ]
  },
  "ForOfStatement": {
    "build": [
      "left",
      "right",
      "body"
    ]
  },
  "YieldExpression": {
    "build": [
      "argument",
      "delegate"
    ]
  },
  "GeneratorExpression": {
    "build": [
      "body",
      "blocks",
      "filter"
    ]
  },
  "ComprehensionBlock": {
    "build": [
      "left",
      "right",
      "each"
    ]
  },
  "ComprehensionExpression": {
    "build": [
      "body",
      "blocks",
      "filter"
    ]
  },
  "PropertyPattern": {
    "build": [
      "key",
      "pattern"
    ]
  },
  "ArrayPattern": {
    "build": [
      "elements"
    ]
  },
  "SpreadElement": {
    "build": [
      "argument"
    ]
  },
  "AssignmentPattern": {
    "build": [
      "left",
      "right"
    ]
  },
  "ClassPropertyDefinition": {
    "build": [
      "definition"
    ]
  },
  "ClassDeclaration": {
    "build": [
      "id",
      "body",
      "superClass"
    ]
  },
  "ClassExpression": {
    "build": [
      "id",
      "body",
      "superClass"
    ]
  },
  "ImportDeclaration": {
    "build": [
      "specifiers",
      "source",
      "importKind"
    ]
  },
  "TaggedTemplateExpression": {
    "build": [
      "tag",
      "quasi"
    ]
  },
  "TemplateLiteral": {
    "build": [
      "quasis",
      "expressions"
    ]
  },
  "TemplateElement": {
    "build": [
      "value",
      "tail"
    ]
  },
  "SpreadPropertyPattern": {
    "build": [
      "argument"
    ]
  },
  "AwaitExpression": {
    "build": [
      "argument",
      "all"
    ]
  },
  "ImportExpression": {
    "build": [
      "source"
    ]
  },
  "ExportBatchSpecifier": {
    "build": []
  },
  "ExportDeclaration": {
    "build": [
      "default",
      "declaration",
      "specifiers",
      "source"
    ]
  },
  "Block": {
    "build": [
      "value",
      "leading",
      "trailing"
    ]
  },
  "Line": {
    "build": [
      "value",
      "leading",
      "trailing"
    ]
  },
  "Flow": {
    "build": []
  },
  "FlowType": {
    "build": []
  },
  "AnyTypeAnnotation": {
    "build": []
  },
  "EmptyTypeAnnotation": {
    "build": []
  },
  "MixedTypeAnnotation": {
    "build": []
  },
  "VoidTypeAnnotation": {
    "build": []
  },
  "NumberTypeAnnotation": {
    "build": []
  },
  "NumberLiteralTypeAnnotation": {
    "build": [
      "value",
      "raw"
    ]
  },
  "NumericLiteralTypeAnnotation": {
    "build": [
      "value",
      "raw"
    ]
  },
  "StringTypeAnnotation": {
    "build": []
  },
  "StringLiteralTypeAnnotation": {
    "build": [
      "value",
      "raw"
    ]
  },
  "BooleanTypeAnnotation": {
    "build": []
  },
  "BooleanLiteralTypeAnnotation": {
    "build": [
      "value",
      "raw"
    ]
  },
  "NullableTypeAnnotation": {
    "build": [
      "typeAnnotation"
    ]
  },
  "NullLiteralTypeAnnotation": {
    "build": []
  },
  "NullTypeAnnotation": {
    "build": []
  },
  "ThisTypeAnnotation": {
    "build": []
  },
  "ExistsTypeAnnotation": {
    "build": []
  },
  "ExistentialTypeParam": {
    "build": []
  },
  "FunctionTypeAnnotation": {
    "build": [
      "params",
      "returnType",
      "rest",
      "typeParameters"
    ]
  },
  "FunctionTypeParam": {
    "build": [
      "name",
      "typeAnnotation",
      "optional"
    ]
  },
  "TypeParameterDeclaration": {
    "build": [
      "params"
    ]
  },
  "ArrayTypeAnnotation": {
    "build": [
      "elementType"
    ]
  },
  "ObjectTypeAnnotation": {
    "build": [
      "properties",
      "indexers",
      "callProperties"
    ]
  },
  "ObjectTypeProperty": {
    "build": [
      "key",
      "value",
      "optional"
    ]
  },
  "ObjectTypeSpreadProperty": {
    "build": [
      "argument"
    ]
  },
  "ObjectTypeIndexer": {
    "build": [
      "id",
      "key",
      "value"
    ]
  },
  "ObjectTypeCallProperty": {
    "build": [
      "value"
    ]
  },
  "ObjectTypeInternalSlot": {
    "build": [
      "id",
      "value",
      "optional",
      "static",
      "method"
    ]
  },
  "Variance": {
    "build": [
      "kind"
    ]
  },
  "QualifiedTypeIdentifier": {
    "build": [
      "qualification",
      "id"
    ]
  },
  "GenericTypeAnnotation": {
    "build": [
      "id",
      "typeParameters"
    ]
  },
  "TypeParameterInstantiation": {
    "build": [
      "params"
    ]
  },
  "MemberTypeAnnotation": {
    "build": [
      "object",
      "property"
    ]
  },
  "UnionTypeAnnotation": {
    "build": [
      "types"
    ]
  },
  "IntersectionTypeAnnotation": {
    "build": [
      "types"
    ]
  },
  "TypeofTypeAnnotation": {
    "build": [
      "argument"
    ]
  },
  "TypeParameter": {
    "build": [
      "name",
      "variance",
      "bound"
    ]
  },
  "ClassImplements": {
    "build": [
      "id"
    ]
  },
  "InterfaceTypeAnnotation": {
    "build": [
      "body",
      "extends"
    ]
  },
  "InterfaceExtends": {
    "build": [
      "id"
    ]
  },
  "InterfaceDeclaration": {
    "build": [
      "id",
      "body",
      "extends"
    ]
  },
  "DeclareInterface": {
    "build": [
      "id",
      "body",
      "extends"
    ]
  },
  "TypeAlias": {
    "build": [
      "id",
      "typeParameters",
      "right"
    ]
  },
  "OpaqueType": {
    "build": [
      "id",
      "typeParameters",
      "impltype",
      "supertype"
    ]
  },
  "DeclareTypeAlias": {
    "build": [
      "id",
      "typeParameters",
      "right"
    ]
  },
  "DeclareOpaqueType": {
    "build": [
      "id",
      "typeParameters",
      "right",
      "supertype"
    ]
  },
  "TypeCastExpression": {
    "build": [
      "expression",
      "typeAnnotation"
    ]
  },
  "TupleTypeAnnotation": {
    "build": [
      "types"
    ]
  },
  "DeclareVariable": {
    "build": [
      "id"
    ]
  },
  "DeclareFunction": {
    "build": [
      "id"
    ]
  },
  "DeclareClass": {
    "build": [
      "id",
      "body",
      "extends"
    ]
  },
  "DeclareModule": {
    "build": [
      "id",
      "body"
    ]
  },
  "DeclareModuleExports": {
    "build": [
      "typeAnnotation"
    ]
  },
  "DeclareExportDeclaration": {
    "build": [
      "default",
      "declaration",
      "specifiers",
      "source"
    ]
  },
  "DeclareExportAllDeclaration": {
    "build": [
      "source"
    ]
  },
  "FlowPredicate": {
    "build": []
  },
  "InferredPredicate": {
    "build": []
  },
  "DeclaredPredicate": {
    "build": [
      "value"
    ]
  },
  "JSXAttribute": {
    "build": [
      "name",
      "value"
    ]
  },
  "JSXIdentifier": {
    "build": [
      "name"
    ]
  },
  "JSXNamespacedName": {
    "build": [
      "namespace",
      "name"
    ]
  },
  "JSXExpressionContainer": {
    "build": [
      "expression"
    ]
  },
  "JSXMemberExpression": {
    "build": [
      "object",
      "property",
      "computed"
    ]
  },
  "JSXSpreadAttribute": {
    "build": [
      "argument"
    ]
  },
  "JSXElement": {
    "build": [
      "openingElement",
      "closingElement",
      "children"
    ]
  },
  "JSXOpeningElement": {
    "build": [
      "name",
      "attributes",
      "selfClosing"
    ]
  },
  "JSXClosingElement": {
    "build": [
      "name"
    ]
  },
  "JSXFragment": {
    "build": [
      "openingElement",
      "closingElement",
      "children"
    ]
  },
  "JSXText": {
    "build": [
      "value"
    ]
  },
  "JSXOpeningFragment": {
    "build": []
  },
  "JSXClosingFragment": {
    "build": []
  },
  "JSXEmptyExpression": {
    "build": []
  },
  "JSXSpreadChild": {
    "build": [
      "expression"
    ]
  },
  "TSType": {
    "build": []
  },
  "TSHasOptionalTypeParameterInstantiation": {
    "build": []
  },
  "TSTypeReference": {
    "build": [
      "typeName",
      "typeParameters"
    ]
  },
  "TSTypeParameterInstantiation": {
    "build": [
      "params"
    ]
  },
  "TSHasOptionalTypeParameters": {
    "build": []
  },
  "TSTypeParameterDeclaration": {
    "build": [
      "params"
    ]
  },
  "TSHasOptionalTypeAnnotation": {
    "build": []
  },
  "TSQualifiedName": {
    "build": [
      "left",
      "right"
    ]
  },
  "TSAsExpression": {
    "build": [
      "expression",
      "typeAnnotation"
    ]
  },
  "TSNonNullExpression": {
    "build": [
      "expression"
    ]
  },
  "TSAnyKeyword": {
    "build": []
  },
  "TSBigIntKeyword": {
    "build": []
  },
  "TSBooleanKeyword": {
    "build": []
  },
  "TSNeverKeyword": {
    "build": []
  },
  "TSNullKeyword": {
    "build": []
  },
  "TSNumberKeyword": {
    "build": []
  },
  "TSObjectKeyword": {
    "build": []
  },
  "TSStringKeyword": {
    "build": []
  },
  "TSSymbolKeyword": {
    "build": []
  },
  "TSUndefinedKeyword": {
    "build": []
  },
  "TSUnknownKeyword": {
    "build": []
  },
  "TSVoidKeyword": {
    "build": []
  },
  "TSThisType": {
    "build": []
  },
  "TSArrayType": {
    "build": [
      "elementType"
    ]
  },
  "TSLiteralType": {
    "build": [
      "literal"
    ]
  },
  "TSUnionType": {
    "build": [
      "types"
    ]
  },
  "TSIntersectionType": {
    "build": [
      "types"
    ]
  },
  "TSConditionalType": {
    "build": [
      "checkType",
      "extendsType",
      "trueType",
      "falseType"
    ]
  },
  "TSInferType": {
    "build": [
      "typeParameter"
    ]
  },
  "TSTypeParameter": {
    "build": [
      "name",
      "constraint",
      "default"
    ]
  },
  "TSParenthesizedType": {
    "build": [
      "typeAnnotation"
    ]
  },
  "TSFunctionType": {
    "build": [
      "parameters"
    ]
  },
  "TSConstructorType": {
    "build": [
      "parameters"
    ]
  },
  "TSDeclareFunction": {
    "build": [
      "id",
      "params",
      "returnType"
    ]
  },
  "TSDeclareMethod": {
    "build": [
      "key",
      "params",
      "returnType"
    ]
  },
  "TSMappedType": {
    "build": [
      "typeParameter",
      "typeAnnotation"
    ]
  },
  "TSTupleType": {
    "build": [
      "elementTypes"
    ]
  },
  "TSRestType": {
    "build": [
      "typeAnnotation"
    ]
  },
  "TSOptionalType": {
    "build": [
      "typeAnnotation"
    ]
  },
  "TSIndexedAccessType": {
    "build": [
      "objectType",
      "indexType"
    ]
  },
  "TSTypeOperator": {
    "build": [
      "operator"
    ]
  },
  "TSIndexSignature": {
    "build": [
      "parameters",
      "typeAnnotation"
    ]
  },
  "TSPropertySignature": {
    "build": [
      "key",
      "typeAnnotation",
      "optional"
    ]
  },
  "TSMethodSignature": {
    "build": [
      "key",
      "parameters",
      "typeAnnotation"
    ]
  },
  "TSTypePredicate": {
    "build": [
      "typeAnnotation",
      "parameterName"
    ]
  },
  "TSCallSignatureDeclaration": {
    "build": [
      "parameters",
      "typeAnnotation"
    ]
  },
  "TSConstructSignatureDeclaration": {
    "build": [
      "parameters",
      "typeAnnotation"
    ]
  },
  "TSEnumMember": {
    "build": [
      "id",
      "initializer"
    ]
  },
  "TSTypeQuery": {
    "build": [
      "exprName"
    ]
  },
  "TSImportType": {
    "build": [
      "argument",
      "qualifier",
      "typeParameters"
    ]
  },
  "TSTypeLiteral": {
    "build": [
      "members"
    ]
  },
  "TSTypeAssertion": {
    "build": [
      "typeAnnotation",
      "expression"
    ]
  },
  "TSEnumDeclaration": {
    "build": [
      "id",
      "members"
    ]
  },
  "TSTypeAliasDeclaration": {
    "build": [
      "id",
      "typeAnnotation"
    ]
  },
  "TSModuleBlock": {
    "build": [
      "body"
    ]
  },
  "TSModuleDeclaration": {
    "build": [
      "id",
      "body"
    ]
  },
  "TSImportEqualsDeclaration": {
    "build": [
      "id",
      "moduleReference"
    ]
  },
  "TSExternalModuleReference": {
    "build": [
      "expression"
    ]
  },
  "TSExportAssignment": {
    "build": [
      "expression"
    ]
  },
  "TSNamespaceExportDeclaration": {
    "build": [
      "id"
    ]
  },
  "TSInterfaceBody": {
    "build": [
      "body"
    ]
  },
  "TSExpressionWithTypeArguments": {
    "build": [
      "expression",
      "typeParameters"
    ]
  },
  "TSInterfaceDeclaration": {
    "build": [
      "id",
      "body"
    ]
  },
  "TSParameterProperty": {
    "build": [
      "parameter"
    ]
  }
}
