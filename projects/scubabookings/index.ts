import { Backend, Frontend, Project } from "~/lib/api/Project";
import Models from "~/projects/scubabookings/models";
import Modules from "~/projects/scubabookings/modules";
import Pages from "~/projects/scubabookings/pages";

export default class Scubabookings extends Project {
  constructor() {
    super({
      name: "scubabookings",
      Frontend,
      Backend,
      Pages,
      Models,
      Modules,
    });
  }
}
