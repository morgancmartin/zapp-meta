import Apollo from '~/modules/apollo'
import Axios from '~/modules/axios'
import Caprover from '~/modules/caprover'
import Dotenv from '~/modules/dotenv'
import Eslint from '~/modules/eslint'
import Express from '~/modules/express'
import Git from '~/modules/git'
import Nuxt from '~/modules/nuxt'
import Overmind from '~/modules/overmind'
import Tailwind from '~/modules/tailwind'
import TypeORM from '~/modules/typeorm'
import Yarn from '~/modules/yarn'

export default [
  Git,
  Express,
  TypeORM,
  Apollo,
  Nuxt,
  Axios,
  Eslint,
  Tailwind,
  Dotenv,
  Overmind,
  Caprover,
  Yarn
]
