import BuildPage from "./utils";
import Search from "./search";
import Hero from "~scubabookings/components/Hero";

const Index = BuildPage<{}>(() => ({
  zapp: {
    name: "index",
    components: [Hero.input({})],
  },
}));

export default [Index, Search];
