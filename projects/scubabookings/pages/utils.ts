import { Page as NuxtPage } from "~/modules/nuxt";
import { Mixin } from "ts-mixer";

class Page extends Mixin(NuxtPage) {}

type PageRequirements = Pick<typeof Page, "zapp" | "nuxt">;
export default Page.build<PageRequirements>();
