import BuildCmp from "~scubabookings/components";

const Template = BuildCmp(() => ({
  zapp: {
    name: "Template",
    elementOnly: "template",
    attributes: [],
  },

  tInput(slotName: string) {
    return [Template, [[`v-slot:${slotName}`, ""]]];
  },
}));

export default Template;
