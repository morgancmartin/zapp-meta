import BuildCmp from "~scubabookings/components";

export default BuildCmp<{ src: "string" }>((PropRef, PropAttr) => ({
  zapp: {
    name: "Img",
    attributes: [PropAttr<"src">("src", "string"), ["class", "max-h-full"]],
    container: {
      attributes: [["class", "object-cover max-h-full overflow-y-hidden"]],
    },
  },
  nuxt: {
    elementName: "img",
  },
}));
