import { Mixin } from "ts-mixer";
import { Component as NuxtComponent } from "~/modules/nuxt";
import { Component as TailwindComponent } from "~/modules/tailwind/Component";

class Component extends Mixin(TailwindComponent, NuxtComponent) {}

type ComponentRequirements = Pick<
  typeof Component,
  "zapp" | "nuxt" | "tailwind"
>;

export default Component.build<ComponentRequirements>();
