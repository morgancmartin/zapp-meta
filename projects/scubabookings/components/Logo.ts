import BuildCmp from "~scubabookings/components";
import Img from "~scubabookings/components/Img";

export default BuildCmp(() => ({
  zapp: {
    name: "Logo",
    components: [
      Img.input({
        src: "logo-scubago.svg",
      }),
    ],
  },
}));
