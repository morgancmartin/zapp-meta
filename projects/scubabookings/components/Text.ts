import BuildCmp from "~scubabookings/components";

export default BuildCmp(() => ({
  zapp: {
    name: "Text",
  },
  tailwind: {},
  nuxt: {
    elementName: "p",
  },
}));
