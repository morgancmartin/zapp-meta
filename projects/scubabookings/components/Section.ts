import BuildCmp from "~scubabookings/components";
import Slot from "~scubabookings/components/Slot";

export default BuildCmp(() => ({
  zapp: {
    name: "Section",
    components: [Slot.input({ name: "contents" })],
  },
}));
