import BuildCmp from "~scubabookings/components";

export default BuildCmp<{ text: "string" }>((PropRef) => ({
  zapp: {
    name: "Button",
    attributes: [["@click", `$emit('click', $event.target.value)`]],
    text: PropRef<"text">("text", "string"),
  },
  nuxt: {
    elementName: "button",
  },
}));
