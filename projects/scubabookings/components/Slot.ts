import BuildCmp from "~scubabookings/components";

export default BuildCmp<{ name: "string" }>((PropRef, PropAttr) => ({
  zapp: {
    name: "Slot",
    elementOnly: "slot",
    attributes: [PropAttr<"name">("name", "string")],
  },
}));
