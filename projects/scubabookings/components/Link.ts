import BuildCmp from "~scubabookings/components";

export default BuildCmp<{ href: "string"; text: "string" }>(
  (PropRef, PropAttr) => ({
    zapp: {
      name: "Link",
      attributes: [PropAttr<"href">("href", "string")],
      text: PropRef<"text">("text", "string"),
    },
  })
);
