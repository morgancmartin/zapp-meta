import BuildCmp from "~scubabookings/components";

// destination/liveaboard/divecenter on the landing page
export default BuildCmp(() => ({
  zapp: {
    name: "PreviewCard",
  },
  tailwind: {},
}));
