import BuildCmp from "~scubabookings/components";
import Link from "~scubabookings/components/Link";

export default BuildCmp<{ href: "string"; text: "string" }>(
  (PropRef, PropAttr) => ({
    zapp: {
      name: "HeaderLink",
      components: [
        Link.input({
          href: PropRef<"href">("href", "string"),
          text: PropRef<"text">("text", "string"),
        }),
      ],
    },
  })
);
