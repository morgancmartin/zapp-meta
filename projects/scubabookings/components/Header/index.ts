import BuildCmp from "~scubabookings/components";
import Link from "./Link";
import Btn from "./Btn";
import Logo from "~scubabookings/components/Logo";

export default BuildCmp(() => ({
  zapp: {
    name: "Header",
    components: [
      Logo.input({}),
      Link.input({
        href: "/",
        text: "Liveaboards",
      }),
      Link.input({
        href: "/",
        text: "Dive Centers",
      }),
      Btn.input({
        text: "US$",
      }),
      Btn.input({
        text: "Log in",
      }),
      Btn.input({
        text: "Register",
      }),
    ],
    attributes: [["class", "flex"]],
  },
  tailwind: {
    height: {
      weight: 12,
    },
  },
}));
