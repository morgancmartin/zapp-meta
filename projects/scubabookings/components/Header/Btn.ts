import BuildCmp from "~scubabookings/components";
import Button from "~scubabookings/components/Button";

export default BuildCmp<{ text: "string" }>((PropRef, PropAttr) => ({
  zapp: {
    name: "HeaderBtn",
    components: [Button.input({ text: PropRef<"text">("text", "string") })],
  },
}));
