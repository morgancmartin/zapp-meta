import BuildCmp from "~scubabookings/components";
import Img from "~scubabookings/components/Img";
import Header from "~scubabookings/components/Header";
// import Template from "~scubabookings/components/Template";
import Section from "~scubabookings/components/Section";

export default BuildCmp(() => ({
  zapp: {
    name: "Hero",
    components: [
      Header.input({}),
      Img.input({ src: "hero-bg.jpeg" }),
      Section.input({}, []),
    ],
    attributes: [["class", "overflow-y-hidden"]],
  },
  tailwind: {
    height: {
      screen: true,
    },
    width: {
      full: true,
    },
  },
}));
