import { Entity } from '~/modules/typeorm'
import { Mixin } from 'ts-mixer'

class Model extends Mixin(class { }, Entity) { }

class Liveaboard extends Model {
  static get zapp(): typeof Model['zapp'] {
    return {
      name: 'liveaboard',
      relations: {
        destination: {
          name: 'destination',
          kind: 'ManyToOne',
          foreignModel: Destination,
          options: {
            onDelete: 'CASCADE'
          }
        },
        images: {
          name: 'images',
          kind: 'OneToMany',
          foreignModel: Image,
          options: {
            cascade: 'update'
          }
        }
      }, columns: {
        name: {
          name: 'name',
          type: 'string'
        }
      }
    }
  }
}

class Destination extends Model {
  static zapp: typeof Model['zapp'] = {
    name: 'destination',
    relations: {}
  }
}

class Image extends Model {
  static zapp: typeof Model['zapp'] = {
    name: 'image',
    relations: {}
  }
}

export default [
  Liveaboard,
  Destination,
  Image
]
