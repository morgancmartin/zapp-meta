import { Directory, File } from '~/lib/api/Filesystem'
import { readFile } from '~/utils/fs'
import recast from '~/utils/recast'

export async function test() {
  const path = '/home/morgan/workspace/zapp-meta/test/source.ts'
  let { data: source } = await readFile(path)
  if (source) {
    const testDir = new TestDir('/home/morgan/workspace/zapp-meta/test/')
    const testFile = new TestFile('metaSource', 'ts')
    testFile.contents = recast.prettyPrint(recast.sourceToMeta(source))
    // testFile.contents = JSON.stringify(recast.parse(source))
    testDir.withFiles([testFile])
    testDir.write()
  }
}

class TestDir extends Directory {
  name: string

  constructor(name: string) {
    super()
    this.name = name
  }
}

class TestFile extends File {
  name: string
  extension?: string
  contents = ''

  constructor(name: string, extension?: string) {
    super()
    this.name = name
    this.extension = extension
  }

  getContents() {
    return this.contents
  }
}
