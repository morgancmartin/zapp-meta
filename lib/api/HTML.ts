import { indentLines, Serializable } from "~/utils/strings";

export class HTMLElement {
  // @ts-ignore
  abstract name: string;
  attributes: HTMLAttribute[] = [];
  children: (Serializable | string)[] = [];
  isSelfClosing?: boolean;
  indentLevel: number = 2;
  classAttr?: HTMLAttribute;
  childSeparator: string = "\n";

  static new(name: string, indentLevel = 2, childSeparator = "\n") {
    const self = new HTMLElement();
    self.name = name;
    self.indentLevel = indentLevel;
    self.childSeparator = childSeparator;
    return self;
  }

  withChild(name: string): HTMLElement {
    return this.withChildElement(HTMLElement.new(name));
  }

  withChildElement(child: HTMLElement): HTMLElement {
    this.children.push(child);
    return child;
  }

  withText(text: string): this {
    this.children.push(text);
    return this;
  }

  withAttr(key: string, value?: string): this {
    if (key === "class" && value && this.classAttr?.value) {
      this.classAttr.value = `${this.classAttr.value} ${value}`;
    } else {
      const attr = new HTMLAttribute(key, value);
      this.attributes.push(attr);
      if (key === "class") {
        this.classAttr = attr;
      }
    }
    return this;
  }

  withClass(value: string): this {
    return this.withAttr("class", value);
  }

  selfClose(): this {
    this.isSelfClosing = true;
    return this;
  }

  toString(): string {
    return HTMLElement.toString(this);
  }

  static toString(self: HTMLElement): string {
    if (self.isSelfClosing) {
      const openTag = `<${self.name}${self.getAttributesString()}/>`;
      return `${openTag}${self.getChildrenString()}`;
    } else {
      const openTag = `<${self.name}${self.getAttributesString()}>`;
      const closeTag = `</${self.name}>`;
      return `${openTag}${self.getChildrenString()}${closeTag}`;
    }
  }

  getAttributesString(): string {
    const attrsLength = this.attributes.reduce(
      (total, attr) => total + attr.getLength(),
      0
    );
    const breaks = attrsLength > 40;
    const joinChar = breaks ? "\n  " : " ";
    const attributes = this.attributes
      .map((attr) => attr.toString())
      .join(joinChar);
    const lastChar = breaks ? "\n" : this.isSelfClosing ? " " : "";
    return attributes
      ? `${joinChar}${attributes}${lastChar}`
      : this.isSelfClosing
      ? " "
      : "";
  }

  getChildrenString(): string {
    const children = this.children
      .map((child: any) => child.toString())
      .join(this.childSeparator);
    return children ? `\n${indentLines(children, this.indentLevel)}\n` : "";
  }
}

export class HTMLAttribute {
  key: string;
  value?: string;

  constructor(key: string, value?: string) {
    this.key = key;
    this.value = value;
  }

  toString(): string {
    return this.value ? `${this.key}="${this.value}"` : `${this.key}`;
  }

  getLength(): number {
    const valLength = this.value ? this.value.length : 0;
    return this.key.length + valLength;
  }
}
