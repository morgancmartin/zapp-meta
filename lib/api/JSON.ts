
import { isArray, isPlainObject } from 'lodash'
import { File } from '~/lib/api/Filesystem'

export class JSONFile extends File {
  // @ts-ignore
  abstract name: string
  extension = 'json'
  // @ts-ignore
  abstract contents: JSONObj

  mergeWith(obj: JSONObj) {
    this.mergeJsonValues(obj, this.contents)
  }

  mergeJsonValues(source: JSONObj, destination: JSONObj) {
    for (const property in source) {
      const srcVal = source[property]
      const destVal = destination[property]

      type testFn = (value: JSON) => boolean
      const bothComplex = (test: testFn) => test(srcVal) && test(destVal)
      const isComplex = bothComplex(isPlainObject) || bothComplex(isArray)

      if (isComplex) {
        this.mergeJsonValues(srcVal as JSONObj, destVal as JSONObj)
      } else {
        destination[property] = srcVal
      }
    }
  }

  getContents(): string {
    return JSON.stringify(this.contents, null, 2)
  }
}

type JSON =
  | string
  | number
  | boolean
  | null
  | { [property: string]: JSON }
  | JSON[]

export type JSONObj = { [property: string]: JSON }
