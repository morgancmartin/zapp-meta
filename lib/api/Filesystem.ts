import * as fs from 'fs'
import { Mixin, hasMixin } from 'ts-mixer'
import { bash } from '~/utils/fs'

export class File {
  // @ts-ignore
  abstract name: string
  extension?: string
  // @ts-ignore
  abstract getContents(): string | Buffer

  get filename() {
    return this.extension ? `${this.name}.${this.extension}` : this.name
  }

  write(parentPath: string) {
    const path = `${parentPath}/${this.filename}`
    const contents = this.getContents()
    const write = () => {
      fs.writeFile(path, contents, (err) => {
        if (err) throw err
      })
    }
    if (!fs.existsSync(path)) {
      write()
    } else {
      fs.readFile(path, (err, data) => {
        if (err) throw err
        if (String(data) !== contents) {
          write()
        }
      })
    }
  }
}

export class Directory {
  // @ts-ignore
  abstract name: string
  contents: (File | Directory)[] = []
  srcPath?: string

  write(parentPath?: string) {
    const path = parentPath ? `${parentPath}/${this.name}` : this.name
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path)
    }
    this.contents.forEach(file => file.write(path))
    if (this.srcPath && fs.existsSync(this.srcPath)) {
      bash(`cp -avr ${this.srcPath}/* ${path}`)
    } else if (this.srcPath) {
      throw new Error(`Invalid Directory srcPath: ${this.srcPath}`)
    }
  }

  hasDir(dirName: string): boolean {
    const isDir = (file: File | Directory) => file instanceof Directory || hasMixin(file, Directory)
    const hasDirName = (file: Directory) => file.name === dirName
    const directories: Directory[] = this.contents.filter(isDir) as Directory[]
    const directory = directories.find(hasDirName)
    return Boolean(directory)
  }

  findFile(fileName: string): File | undefined {
    const result = this.contents
      .filter((file: any) => file instanceof File || hasMixin(file, File))
      .find((file: any) => file.name === fileName)

    return (result && (result instanceof File || hasMixin(result, File))) ? result : undefined
  }

  findDirectory(directoryName: string): Directory | undefined {
    const result = this.contents
      .filter((dir: any) => dir instanceof Directory || hasMixin(dir, Directory))
      .find((dir: any) => dir.name === directoryName)
    return (result && (result instanceof Directory || hasMixin(result, Directory))) ? result : undefined
  }

  withFiles(files: (File | Directory)[]): void {
    this.contents = this.contents.filter((file: File | Directory) => {
      return !files.find((newFile: File | Directory) => {
        return typeof file === typeof newFile && file.name === newFile.name
      })
    }).concat(files)
  }

  copyFiles(path: string) { }
}

export class Sourceable {
  // @ts-ignore
  abstract name: string
  // @ts-ignore
  abstract path: string
  private _sourceContents?: Buffer

  get sourceContents(): Buffer {
    if (!this._sourceContents) {
      this._sourceContents = fs.readFileSync(this.path)
    }
    return this._sourceContents
  }
}

export class FSFile extends Mixin(Sourceable, File) {
  // @ts-ignore
  abstract path: Sourceable['path']

  getContents(): Buffer {
    return this.sourceContents
  }
}