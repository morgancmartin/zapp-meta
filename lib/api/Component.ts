import { Project } from "./Project";
import { Named } from "~/utils/strings";
import { uniqBy, uniq } from "lodash";

export class Component extends Named {
  project: Project;

  static zapp: {
    name: string;
    elementOnly?: string;
    components?: AnyComponentInput[];
    attributes?: Attribute[];
    text?: string | PropRef<"string">;
    container?: {
      attributes?: Attribute[];
    };
  };

  zapp: typeof Component["zapp"] & {
    props: Prop<string, PropTypeStrings>[];
  };

  constructor(project: Project, extension: typeof Component) {
    super();
    this.project = project;
    this.zapp = {
      ...extension.zapp,
      props: extension.props,
    };
  }

  static withClass(name: string) {
    this.withAttr(["class", name]);
  }

  static withAttr(attr: Attribute, overwrite = false) {
    if (!this.zapp.attributes) {
      this.zapp.attributes = [];
    }
    const attrs = this.zapp.attributes;
    const previous = attrs.find(
      (previous: Attribute) => previous[0] === attr[0]
    );
    if (previous) {
      if (overwrite) {
        attrs.splice(attrs.indexOf(previous), 1, attr);
      } else {
        previous[1] = [previous[1], attr[1]].join(" ");
      }
    } else {
      attrs.push(attr);
    }
  }

  static get uniqComponentClasses(): typeof Component[] {
    return uniq(this.zapp.components ?? [])
      .filter((cmpInput: ComponentInput) => !cmpInput[0].zapp?.elementOnly)
      .map((cmpInput: ComponentInput) => cmpInput[0]);
  }

  static get propNames() {
    return (
      this.props.map((prop: Prop<string, PropTypeStrings>) => prop.name) ?? []
    );
  }

  static get propValuesByName(): {
    [name: string]: Prop<string, PropTypeStrings>;
  } {
    return (
      this.props.reduce(
        (
          props: { [name: string]: Prop<string, PropTypeStrings> },
          prop: Prop<string, PropTypeStrings>
        ) => ({
          [prop.name]: prop,
        }),
        {}
      ) ?? {}
    );
  }

  static get propTypeToLiteral() {
    return {
      string: "str",
      boolean: true,
    };
  }

  static props: AnyComponent["zapp"]["props"];

  static propInputSpec<C extends typeof Component>(): [
    keyof C["propValuesByName"],
    LiteralTypes
  ][] {
    return (
      this.props.map((prop: Prop<string, PropTypeStrings>) => [
        prop.name,
        this.propTypeToLiteral[prop.type],
      ]) ?? []
    );
  }

  static build<R extends {}>() {
    const Self: typeof Component = this;
    return <P extends PropInput>(getInput: InputGetter<R, P>) => {
      let extension: typeof Self = Object.assign(class extends Self {}, {
        props: [],
      });
      const input: R = getInput(
        extension.PropRefFor<P>(),
        extension.PropAttrFor<P>()
      );
      return Object.assign(extension, input, {
        input(
          values: PropFulfill<P>,
          children?: ComponentInput[]
        ): ComponentInput {
          const attrs: Attribute[] = Object.entries(
            values
          ).map(([key, value]: [string, LiteralTypes | AnyPropRef]) => [
            typeof value === "string" ? key : `:${key}`,
            serializeAttrVal(value),
          ]);
          return children ? [extension, attrs, children] : [extension, attrs];
        },
      });
    };
  }

  static PropRefFor<P extends PropInput>() {
    return <N extends keyof P & string>(
      propName: N,
      propType: P[N] & PropTypeStrings
    ): PropRef<P[N]> => {
      return this.PropRef<P, N>(propName, propType);
    };
  }

  static PropRef<P extends PropInput, N extends keyof P & string>(
    propName: N,
    propType: P[N] & PropTypeStrings
  ): PropRef<P[N]> {
    const propRef: PropRef<P[N]> = {
      propRef: true,
      propName,
      propType,
    };
    this.props.push(propRefToProp(propRef));
    return propRef;
  }

  static PropAttrFor<P extends PropInput>() {
    return <N extends keyof P & string>(
      key: N,
      propType: P[N] & PropTypeStrings
    ): Attribute => {
      return this.PropAttr<P, N>(key, propType);
    };
  }

  static PropAttr<P extends PropInput, N extends keyof P & string>(
    key: N,
    propType: P[N] & PropTypeStrings
  ): Attribute {
    return [`:${key}`, this.PropRef(key, propType)];
  }

  get name(): string {
    return this.zapp.name;
  }

  get components(): ComponentInput[] {
    return this.zapp.components ?? [];
  }

  get attributes(): Attribute[] {
    return this.zapp.attributes ?? [];
  }

  get propsByName(): PropsByName {
    return this.zapp.props.reduce(
      (props: PropsByName, prop: AnyProp) => ({
        ...props,
        [prop.name]: prop,
      }),
      {}
    );
  }

  get uniqComponents(): ComponentInput[] {
    return uniqBy(
      this.components ?? [],
      (cmpInput: ComponentInput) => cmpInput[0].zapp.name
    );
  }

  get uniqComponentClasses(): typeof Component[] {
    return this.uniqComponents
      .filter((cmpInput: ComponentInput) => !cmpInput[0].zapp?.elementOnly)
      .map((cmpInput: ComponentInput) => cmpInput[0]);
  }
}

export interface PropInput {
  [name: string]: PropTypeStrings;
}

type PropFulfill<I extends PropInput> = {
  [P in keyof I]: PropTypeToLiteral<I[P]> | PropRef<I[P]>;
};

export type AnyComponent = Component;

export type AnyComponentInput = ComponentInput;

type ComponentInput =
  | BaseComponentInput
  | [typeof Component, Attribute[], ComponentInput[]];

type BaseComponentInput = [typeof Component, Attribute[]];

type PropTypeStrings = "string" | "boolean";

const propTypeStrings: PropTypeStrings[] = ["string", "boolean"];

type PropTypeToLiteral<T extends PropTypeStrings> = {
  string: string;
  boolean: boolean;
}[T];

type LiteralTypes = string | boolean;

export interface Prop<N extends string, T extends PropTypeStrings> {
  name: N;
  type: T;
  required?: boolean;
  default?: any;
}

type AnyProp = Prop<string, PropTypeStrings>;

type PropsByName = { [name: string]: Prop<string, PropTypeStrings> };

export interface PropRef<T extends PropTypeStrings | PropRef<PropTypeStrings>> {
  propRef: true;
  propName: string;
  propType: T;
}

function isPropRef(maybePropRef: AnyPropRef | any): AnyPropRef | undefined {
  const isAPropRef =
    maybePropRef?.propRef === true &&
    typeof maybePropRef?.propName === "string" &&
    propTypeStrings.includes(maybePropRef?.propType);

  return isAPropRef ? (maybePropRef as AnyPropRef) : undefined;
}

function propRefToProp(propRef: AnyPropRef): AnyProp {
  return { name: propRef.propName, type: propRef.propType };
}

export type AnyPropRef = PropRef<PropTypeStrings>;

function serializeAttrVal(value: string | boolean | AnyPropRef): string {
  const propRef: ReturnType<typeof isPropRef> = isPropRef(value);
  if (propRef) {
    return propRef.propName;
  } else if (typeof value === "string") {
    return value;
  } else {
    return `${value}`;
  }
}

type AttributeValue = string | AnyPropRef;

export type Attribute = [string, AttributeValue];

type InputGetter<R extends {}, P extends PropInput> = (
  prop: <N extends keyof P & string>(
    propName: N,
    propType: P[N] & PropTypeStrings
  ) => PropRef<P[N]>,
  attr: <N extends keyof P & string>(
    key: N,
    propType: P[N] & PropTypeStrings
  ) => Attribute
) => R;
