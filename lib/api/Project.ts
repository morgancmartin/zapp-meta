import { Mixin } from "ts-mixer";
import { Component, AnyComponent } from "~/lib/api/Component";
import { Model } from "~/lib/api/Model";
import { Module } from "~/lib/api/Module";
import { NodeProject } from "~/lib/api/NodeProject";
import { Page, AnyPage } from "~/lib/api/Page";
// import { uniqBy } from "lodash";

export class Frontend extends Mixin(class {}, NodeProject) {
  constructor(projectName: string) {
    super(projectName, "web");
  }
}

export class Backend extends Mixin(class {}, NodeProject) {
  constructor(projectName: string) {
    super(projectName, "api");
  }
}

export class Project {
  name: string;
  path: string = "/home/morgan/.zapp-meta";
  frontend: Frontend;
  backend: Backend;
  pages: ProjectCollection<AnyPage>;
  components: ProjectCollection<AnyComponent>;
  models: ProjectCollection<Model>;
  modules: ProjectCollection<Module>;

  constructor({
    name,
    Frontend,
    Backend,
    Pages,
    Models,
    Modules,
  }: ProjectInput) {
    this.name = name;
    this.frontend = new Frontend(name);
    this.backend = new Backend(name);
    this.modules = this.buildProjectModules(Modules);
    this.models = this.buildCollection<ProjectInput["Models"], typeof Model>(
      Models
    );
    this.components = this.buildCollection<
      typeof Component[],
      typeof Component
    >(this.findAllComponents(Pages));
    this.pages = this.buildCollection<ProjectInput["Pages"], typeof Page>(
      Pages
    );
    this.completeModules(Modules);
  }

  buildCollection<
    I extends Array<typeof Model | typeof Component | typeof Page>,
    StaticKind extends typeof Model | typeof Component | typeof Page
  >(items: I): ProjectCollection<InstanceType<StaticKind>> {
    return items.reduce(
      (
        collection: ProjectCollection<InstanceType<StaticKind>>,
        Klass: any
      ) => ({
        ...collection,
        [Klass.zapp.name]: new Klass(this, Klass),
      }),
      {}
    );
  }

  buildProjectModules(modules: ProjectInput["Modules"]): ProjectModules {
    return modules.reduce((mods: ProjectModules, ModClass: typeof Module) => {
      const mod = new ModClass(this, ModClass);
      return {
        ...mods,
        [ModClass.modname]: mod,
      };
    }, {});
  }

  findAllComponents(pages: typeof Page[]) {
    return uniqComponents(
      pages.reduce(
        (cmps: typeof Component[], page: typeof Page) => [
          ...cmps,
          ...this.findPageComponents(page),
        ],
        []
      )
    );
  }

  findPageComponents(page: typeof Page) {
    return uniqComponents([
      ...page.uniqComponentClasses.reduce(
        (cmps: typeof Component[], Cmp: typeof Component) => [
          ...cmps,
          ...this.findComponents(Cmp),
        ],
        []
      ),
    ]);
  }

  findComponents(Cmp: typeof Component): typeof Component[] {
    return uniqComponents([
      Cmp,
      ...Cmp.uniqComponentClasses.reduce(
        (cmps: typeof Component[], cmp: typeof Component) => [
          ...cmps,
          ...this.findComponents(cmp),
        ],
        []
      ),
    ]);
  }

  completeModules(Modules: ProjectInput["Modules"]) {
    Object.values(Modules).forEach((mod: typeof Module) => {
      if (mod.complete) {
        mod.complete();
      }
    });
  }

  write() {
    this.frontend.write(this.path);
    this.backend.write(this.path);
  }
}

type ProjectCollection<T> = { [name: string]: T };
type ProjectModules = ProjectCollection<Module>;

function uniqComponents(Components: typeof Component[]) {
  return Components;
  // return uniqBy(Components, (Cmp: typeof Component) => {
  //   console.log(Cmp.zapp.name);
  //   return Cmp.zapp.name;
  // });
}

export interface ProjectInput {
  name: string;
  Frontend: typeof Frontend;
  Backend: typeof Backend;
  Pages: typeof Page[];
  Models: typeof Model[];
  Modules: any[];
}
