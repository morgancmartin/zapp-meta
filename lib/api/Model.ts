import { Named } from '~/utils/strings'
import { Project } from './Project'

export class Model extends Named {
  static zapp: {
    name: string
    relations?: { [name: string]: Relation }
    columns?: { [name: string]: Column }
  }

  extension: typeof Model
  get name(): string {
    return this.extension.zapp.name
  }

  _columns?: Column[]

  get columns() {
    return this._columns ?? []
  }

  set columns(columns: Column[]) {
    this._columns = columns
  }

  _relations?: Relation[]

  get relations() {
    return this._relations ?? []
  }
  set relations(relations: Relation[]) {
    relations.forEach((rel: Relation) => {
      const complement = RelationKindComplements[rel.kind]
      const foreignRelations = rel.foreignModel.zapp.relations
      const foreignProp = rel.foreignProp
        ? rel.foreignProp
        : complement === 'OneToMany'
          ? this.pluralName
          : this.name

      if (foreignRelations && !foreignRelations[foreignProp]) {
        foreignRelations[foreignProp] = {
          name: foreignProp,
          kind: complement,
          foreignModel: this.extension,
          foreignProp: rel.name
        }
      }
    })
    this._relations = relations
  }

  constructor(project: Project, extension: typeof Model) {
    super()
    this.extension = extension
    const input = extension.zapp
    if (input.relations) {
      this.relations = Object.values(input.relations)
    }
    if (input.columns) {
      this.columns = Object.values(input.columns)
    }
  }
}

enum RelationKindComplements {
  ManyToOne = 'OneToMany',
  OneToMany = 'ManyToOne'
}

type RelationKind =
  | 'ManyToOne'
  | 'OneToMany'

interface Relation {
  name: string
  kind: RelationKind
  foreignModel: typeof Model
  foreignProp?: string
  options?: {
    cascade?: boolean | 'insert' | 'update'
    onDelete?: 'CASCADE'
  }
}

interface Column {
  name: string
  type: 'string' | 'boolean'
}