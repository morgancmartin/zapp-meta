import { Directory } from '~/lib/api/Filesystem'
import { JSONFile, JSONObj } from '~/lib/api/JSON'
import { Readme } from '~/lib/api/Markdown'

export class NodeProject extends Directory {
  readme: Readme = new Readme()
  packageJson: PackageJson
  name: string
  projectName: string
  nameSuffix: string

  constructor(projectName: string, nameSuffix: string) {
    super()
    this.projectName = projectName
    this.nameSuffix = nameSuffix
    this.name = `${projectName}-${nameSuffix}`
    this.packageJson = new PackageJson(this.name)
    this.withFiles([
      this.packageJson,
      this.readme
    ])
  }
}

export class PackageJson extends JSONFile {
  name = 'package'
  contents: JSONObj

  constructor(projectName: string) {
    super()
    this.contents = {
      name: projectName,
      version: '1.0.0',
      author: 'Morgan Martin',
      private: true,
      scripts: {},
      dependencies: {},
      devDependencies: {}
    }
  }
}

export class NodeProjectContributor {
  integratePackageJson(projectDir: NodeProject, jsonObj: JSONObj) {
    projectDir.packageJson.mergeWith(jsonObj)
  }
}