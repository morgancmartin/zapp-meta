import { Component } from "~/lib/api/Component";
import { Project } from "./Project";

export class Page extends Component {
  static zapp: typeof Component["zapp"];
  constructor(project: Project, extension: typeof Page) {
    super(project, extension);
  }
}

export type AnyPage = Page;
