import { Mixin } from 'ts-mixer'
import { File } from '~/lib/api/Filesystem'
import { indentLines } from '~/utils/strings'

export type CSS =
  | CSSRule
  | CSSBlock
  | CSSComment
  | CSSImport

export class CSSImport {
  importText: string

  constructor(importText: string) {
    this.importText = importText
  }

  toString(): string {
    return `@import ${this.importText};`
  }
}

export class CSSComment {
  comment: string

  constructor(comment: string) {
    this.comment = comment
  }

  toString(): string {
    return `/* ${this.comment} */`
  }
}

export class CSSParent {
  rules: CSS[] = []

  withRule(property: string, value: string): this {
    this.rules.push(new CSSRule([property, value]))
    return this
  }

  withComment(comment: string) {
    this.rules.push(new CSSComment(comment))
    return this
  }

  withImport(importText: string) {
    this.rules.push(new CSSImport(importText))
    return this
  }

  withBlock(selector: string): CSSBlock {
    const block = new CSSBlock(selector)
    this.rules.push(block)
    return block
  }
}

export class CSSFile extends Mixin(File, CSSParent) {
  // @ts-ignore
  abstract name: string
  extension = 'css'

  getContents(): string {
    return this.toString()
  }
}

export type CSSRuleInput = [string, string]

export class CSSRule {
  property: string
  value: string

  constructor(input: CSSRuleInput) {
    const [property, value] = input
    this.property = property
    this.value = value
  }

  toString(): string {
    return `${this.property}: ${this.value};`
  }
}

export class CSSBlock extends CSSParent {
  selector: string

  constructor(selector: string) {
    super()
    this.selector = selector
  }

  toString(): string {
    const rules = this.rules.map((rule: any) => rule.toString()).join('\n')
    return `${this.selector} {\n${indentLines(rules)}\n}`
  }
}
