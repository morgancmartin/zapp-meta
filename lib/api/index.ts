import * as action from './Action'
import * as component from './Component'
import * as environment from './Environment'
import * as model from './Model'
import * as page from './Page'

export default {
  ...model,
  ...component,
  ...page,
  ...environment,
  ...action
}
