import { mergeWith } from 'lodash';
import { Mixin } from 'ts-mixer';
import { Directory } from '~/lib/api/Filesystem';
import { NodeProject, NodeProjectContributor, PackageJson } from '~/lib/api/NodeProject';
import { Project } from '~/lib/api/Project';

export class Module extends Mixin(class { }, NodeProjectContributor) {
  static modname: string;
  static project: Project

  static get frontend(): ModuleProjectDir { return {} }

  static get backend(): ModuleProjectDir { return {} }

  static complete() { }

  constructor(project: Project, mod: typeof Module) {
    super();
    mod.project = project
    const { frontend = {}, backend = {} } = mod

    if (frontend) {
      this.mergeNodeProject(project, mod, 'frontend');
    }

    if (backend) {
      this.mergeNodeProject(project, mod, 'backend');
    }

  }

  mergeNodeProject(project: Project, mod: typeof Module, key: 'frontend' | 'backend') {
    const input = mod[key]
    const nodeProject: NodeProject = project[key];
    return mergeWith(nodeProject, input, (objValue: any, srcValue: any) => {
      if (Array.isArray(objValue)) {
        return objValue.concat(srcValue);
      }
      else if (objValue instanceof PackageJson) {
        return this.integratePackageJson(nodeProject, srcValue);
      }
    });
  }
}

export type ModuleProjectDir = Partial<
  Pick<Directory, 'contents'> & {
    packageJson: PackageJson['contents']
  }
>
