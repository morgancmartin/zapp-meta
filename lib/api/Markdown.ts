import { File } from '~/lib/api/Filesystem'

export class MarkdownFile extends File {
    // @ts-ignore
    abstract name: string
    extension = 'md'
    contents: string[] = []

    getContents(): string {
        return this.contents.join('\n')
    }
}

export class Readme extends MarkdownFile {
    name = 'README'

    withContents(contents: string[]) {
        this.contents = [...this.contents, ...contents]
    }
}